import React, { Component } from 'react';

import * as Font from 'expo-font';
import { AppLoading } from 'expo';
import { Ionicons } from '@expo/vector-icons';

import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';

import firebase from 'firebase';

import reducers from './src/redux/reducers';
import { Provider } from 'react-redux';
import reduxThunk from 'redux-thunk';
import { createStore, applyMiddleware } from 'redux';

const store = createStore(reducers, {}, applyMiddleware(reduxThunk));

import LoginScreen from './src/screens/LoginScreen';
import RegisterScreen from './src/screens/RegisterScreen';
import SelectScreen from './src/screens/SelectScreen';
import OrderScreen from './src/screens/OrderScreen';
import ReportSummaryScreen from './src/screens/ReportSummaryScreen';

const AuthenNavigator = createSwitchNavigator({
    LoginScreen: { screen: LoginScreen, navigationOptions: { tabBarVisible: false } },
    RegisterScreen: { screen: RegisterScreen, navigationOptions: { tabBarVisible: false } },
});

const SellerNavigator = createBottomTabNavigator({
    SelectScreen: { screen: SelectScreen, navigationOptions: { tabBarVisible: false } },
    ReportSummaryScreen: { screen: ReportSummaryScreen, navigationOptions: { tabBarVisible: false } },
    OrderScreen: { screen: OrderScreen, navigationOptions: { tabBarVisible: false } },
    AuthenScreen: { screen: AuthenNavigator, navigationOptions: { tabBarVisible: false } },
});

const RootNavigator = createBottomTabNavigator({
    AuthenScreen: { screen: AuthenNavigator, navigationOptions: { tabBarVisible: false } },
    SellerScreen: { screen: SellerNavigator, navigationOptions: { tabBarVisible: false } },
});

const Navigator = createAppContainer(RootNavigator);

export default class App extends Component {
    state = { loading: true };

    async componentDidMount() {
        this.firebaseConfig();
        this.loadFonts();
    }

    firebaseConfig() {
        const firebaseConfig = {
            apiKey: 'AIzaSyDQ5O5TME5gZ2v5Lg-mBI2-HB1Kt66b-1o',
            authDomain: 'fresh-market-mobile.firebaseapp.com',
            databaseURL: 'https://fresh-market-mobile.firebaseio.com',
            projectId: 'fresh-market-mobile',
            storageBucket: 'fresh-market-mobile.appspot.com',
            messagingSenderId: '877419697307',
            appId: '1:877419697307:web:0791e9a5d15aaa8f98a23e',
        };
        if (!firebase.apps.length) firebase.initializeApp(firebaseConfig);
    }

    async loadFonts() {
        await Font.loadAsync({
            Roboto: require('native-base/Fonts/Roboto.ttf'),
            Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
            ...Ionicons.font,
        });
        this.setState({ loading: false });
    }

    render() {
        var { loading } = this.state;
        if (loading) {
            return <AppLoading />;
        } else {
            return (
                <Provider store={store}>
                    <Navigator />
                </Provider>
            );
        }
    }
}
