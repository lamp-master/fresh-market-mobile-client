import React, { Component } from 'react';
import { Modal, AppRegistry, Animated, View, FlatList, TextInput, Image, TouchableOpacity } from 'react-native';
import { Text } from 'native-base';
import { ButtonPrimary, ButtonSecondary } from '../components/ButtonsComponent';
import { FontAwesome } from '@expo/vector-icons';
import { styles as gstyles } from '../../styles/styles';
import { database, storage } from 'firebase/app';

import * as Permissions from 'expo-permissions';
import * as ImagePicker from 'expo-image-picker';
import ModalHeader from '../components/ModalHeader';
import RNPickerSelect from 'react-native-picker-select';
import Constants from 'expo-constants';
import Dialog from 'react-native-dialog';

import { connect } from 'react-redux';
import { addItemToCart, updateStock } from '../../redux/actions/OrderAction';

export class VegetableOrderScreen extends Component {
    _isMounted = false;

    state = {
        ingredians: [],
        name: '',
        type: 'ผัก',
        price: 0,
        stock: 0,
        package_type: '',
        modal_visible: false,
        dialog_visible: false,
        edit_stock: false,
        image: '',
        item: {},
        quantity: 0,
    };

    componentWillMount() {
        this.animatedValue = new Animated.Value(0);
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    componentDidMount() {
        this._isMounted = true;
        Animated.timing(this.animatedValue, {
            toValue: 150,
            duration: 1500,
        }).start();
        if (this.state.ingredians.length === 0) {
            this.fetchVegetable();
            this.getPermissionAsync();
        }
    }

    getPermissionAsync = async () => {
        if (Constants.platform.ios) {
            const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
            if (status !== 'granted') {
                alert('Sorry, we need camera roll permissions to make this work!');
            }
        }
    };

    fetchVegetable = () => {
        const VegetableRef = database().ref('ingredians/vegetable');
        VegetableRef.on('value', snapshot => {
            if (snapshot.val()) {
                const ingredian_object = snapshot.val();
                let ingredian_list = Object.keys(ingredian_object).map(key => ({
                    ...ingredian_object[key],
                    uid: key,
                }));
                ingredian_list.push({ name: 'เพิ่มรายการใหม่', uid: '-LttFtLKmOowK7n_8E7i' });
                if (this._isMounted) this.setState({ ingredians: ingredian_list });
            } else {
                if (this._isMounted) this.setState({ ingredians: [{ name: 'เพิ่มรายการใหม่', uid: '12345678901234567890' }] });
            }
        });
    };

    uploadImage = async () => {
        alert('pick');
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.All,
            allowsEditing: true,
            aspect: [4, 3],
            quality: 0.1,
        });

        if (!result.cancelled) {
            result.height = 640;
            result.width = 480;
            this.setState({ image: result.uri });
        }
    };

    addNewIngredian = async () => {
        let { name, type, price, package_type, image, stock } = this.state;

        this.setState({ loading: true });
        // UPLOAD IMAGE TO FIREBASE
        let image_response = await fetch(image);
        let blob = await image_response.blob();
        let image_upload = await storage()
            .ref('ingredians/vegetable')
            .child(name)
            .put(blob);
        let image_uri = await image_upload.ref.getDownloadURL();

        let ingredian = database().ref('ingredians/vegetable');

        await ingredian
            .push({
                name: name,
                type: type,
                price: parseFloat(price),
                package_type: package_type,
                image_uri: image_uri,
                stock: parseFloat(stock),
            })
            .then(() => {
                this.setState({ modal_visible: false });
            });
    };

    addToOrder = () => {
        let { item, quantity, dialog_visible } = this.state;
        item = { ...item, quantity: quantity };
        this.props.addItemToCart(item);
        this.setState({ quantity: 0, item: {} });
        this.toggleDialog({}, !dialog_visible);
    };

    addToStock = () => {
        let { item, stock, dialog_visible } = this.state;
        const VegetableRef = database()
            .ref('ingredians/vegetable')
            .child(item.uid);
        VegetableRef.update({
            stock: parseFloat(item.stock.toFixed(2)) + parseFloat(stock.toFixed(2)),
        });
        this.props.updateStock(item.uid, stock);
        this.setState({ stock: 0, item: {} });
        this.toggleDialog({}, !dialog_visible);
    };

    resetIngredian = () => {
        this.setState({
            name: '',
            price: 0,
            package_type: '',
            modal_visible: false,
            image: '',
        });
    };

    toggleModal(visible) {
        if (visible) {
            this.resetIngredian();
        }
        this.setState({ modal_visible: visible });
    }

    toggleDialog(item, dialog_visible) {
        this.setState({ item: item, dialog_visible: dialog_visible });
    }

    render() {
        let { toggle_edit } = this.props.stock;

        const edited_item = this.animatedValue.interpolate({
            inputRange: [0, 150],
            outputRange: ['powderblue', 'grey'],
        });

        const placeholder = {
            label: 'โปรดระบุหน่วยที่ใช้ (กรัม, ขีด, ...)',
            value: null,
            color: 'grey',
        };
        const { ingredians, modal_visible, dialog_visible, image, type, package_type } = this.state;

        return (
            <View style={[ingredians.length > 3 ? { flex: 1, alignItems: 'center', margin: '0.5%' } : { flex: 1 }]}>
                {/* SHOW VEGETABLE */}
                <FlatList
                    horizontal={false}
                    numColumns={4}
                    contentContainerStyle={{ flex: 1, alignItems: 'stretch', flexDirection: 'column' }}
                    data={ingredians}
                    renderItem={({ item, index }) => (
                        <TouchableOpacity
                            onPress={() => {
                                if (index + 1 === ingredians.length) {
                                    this.toggleModal(!modal_visible);
                                } else {
                                    if (item.stock !== 0 || toggle_edit) this.toggleDialog(item, !dialog_visible);
                                }
                            }}
                            style={{ flexDirection: 'row', width: '23%', height: 230, flexWrap: 'wrap', margin: '1%' }}
                        >
                            <Animated.View
                                style={{
                                    width: '100%',
                                    height: '100%',
                                    padding: 3,
                                    backgroundColor: toggle_edit === true && index + 1 !== ingredians.length ? edited_item : 'powderblue',
                                    borderWidth: 1,
                                    borderColor: 'grey',
                                }}
                            >
                                <View style={{ width: 145, height: 145, alignItems: 'center', justifyContent: 'center' }}>
                                    {item.image_uri ? (
                                        <Image
                                            resizeMode="cover"
                                            style={{ width: 135, height: 135, overflow: 'hidden' }}
                                            source={{
                                                uri: item.image_uri,
                                            }}
                                        />
                                    ) : (
                                        <FontAwesome name="image" size={30} />
                                    )}
                                </View>
                                <Text style={[gstyles.textH4, { alignSelf: 'center', paddingTop: 2.5 }]}>{item.name}</Text>
                                {item.price ? (
                                    <Text style={[gstyles.textH4, { alignSelf: 'center', paddingTop: 2.5 }]}>
                                        {parseFloat(item.price)
                                            .toFixed(2)
                                            .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}{' '}
                                        บาท/{item.package_type}
                                    </Text>
                                ) : (
                                    []
                                )}
                                {item.stock > 0 ? (
                                    <Text style={[gstyles.textH4, { alignSelf: 'center', paddingTop: 2.5 }]}>
                                        คงเหลือ {item.stock} {item.package_type}
                                    </Text>
                                ) : (
                                    <View>{index + 1 !== ingredians.length ? <Text style={[gstyles.textH4, { alignSelf: 'center', paddingTop: 2.5 }]}>(สินค้าหมด)</Text> : []}</View>
                                )}
                            </Animated.View>
                        </TouchableOpacity>
                    )}
                    keyExtractor={item => item.uid}
                />

                {/* ADD TO CART */}
                <Dialog.Container visible={dialog_visible}>
                    <Dialog.Title>{toggle_edit ? 'เพิ่มสต็อก' : 'ระบุจำนวน'}</Dialog.Title>
                    <Dialog.Description>{toggle_edit ? 'โปรดระบุจำนวนที่จะเพิ่มลงสต็อก' : 'โปรดระบุจำนวนที่ต้องการ'}</Dialog.Description>
                    <Dialog.Input
                        onChangeText={value => {
                            if (toggle_edit) {
                                this.setState({ stock: parseFloat(value) });
                            } else {
                                this.setState({ quantity: parseFloat(value) });
                            }
                        }}
                        keyboardType={'numeric'}
                    />
                    <Dialog.Button
                        label="ยกเลิก"
                        onPress={() => {
                            if (toggle_edit) {
                                this.setState({ stock: 0, item: {} });
                                this.toggleDialog({}, !dialog_visible);
                            } else {
                                this.setState({ quantity: 0, item: {} });
                                this.toggleDialog({}, !dialog_visible);
                            }
                        }}
                    />
                    <Dialog.Button
                        label="ยืนยัน"
                        onPress={() => {
                            if (toggle_edit) {
                                this.addToStock();
                            } else {
                                this.addToOrder();
                            }
                        }}
                    />
                </Dialog.Container>

                {/* ADD VEGETABLE */}
                <Modal animationType="fade" transparent={false} presentationStyle="formSheet" visible={modal_visible}>
                    <View style={{ flex: 1 }}>
                        <ModalHeader title="เพิ่มรายการวัตถุดิบ" rightEvent={() => this.toggleModal(!modal_visible)} />
                        <View style={gstyles.container}>
                            <View style={{ flex: 4, alignItems: 'center' }}>
                                <TouchableOpacity
                                    onPress={() => this.uploadImage()}
                                    style={{
                                        borderWidth: 1.5,
                                        overflow: 'hidden',
                                        borderColor: 'black',
                                        flex: 1,
                                        aspectRatio: 1,
                                        margin: 10,
                                        borderRadius: 2.5,
                                    }}
                                >
                                    <View style={{ flex: 1, borderRadius: 5, alignItems: 'center', justifyContent: 'center' }}>
                                        {image ? (
                                            <Image
                                                overflow="hidden"
                                                style={{ flex: 1, overflow: 'hidden', height: '100%', width: '100%' }}
                                                source={{
                                                    uri: image ? image : '',
                                                }}
                                            />
                                        ) : (
                                            <FontAwesome name="image" size={30} />
                                        )}
                                    </View>
                                </TouchableOpacity>
                            </View>
                            <View style={{ flex: 8 }}>
                                <View style={gstyles.formRowElement}>
                                    <View style={{ flex: 3 }}>
                                        <View
                                            style={{
                                                margin: 5,
                                                alignSelf: 'stretch',
                                                alignItems: 'center',
                                                flexDirection: 'row',
                                                justifyContent: 'space-between',
                                                paddingLeft: 15,
                                                paddingRight: 15,
                                                height: 45,
                                            }}
                                        >
                                            <Text style={gstyles.textH2}>ชื่อสินค้า</Text>
                                            <Text style={gstyles.textH2}>:</Text>
                                        </View>
                                    </View>
                                    <View style={{ flex: 9 }}>
                                        <View style={gstyles.inputContainer}>
                                            <TextInput
                                                style={gstyles.inputPrimary}
                                                placeholder="โปรดระบุชื่อสินค้า"
                                                placeholderTextColor="grey"
                                                onChangeText={name => {
                                                    this.setState({ name: name });
                                                }}
                                            />
                                        </View>
                                    </View>
                                </View>
                                <View style={gstyles.formRowElement}>
                                    <View style={{ flex: 3 }}>
                                        <View
                                            style={{
                                                margin: 5,
                                                alignSelf: 'stretch',
                                                alignItems: 'center',
                                                flexDirection: 'row',
                                                justifyContent: 'space-between',
                                                paddingLeft: 15,
                                                paddingRight: 15,
                                                height: 45,
                                            }}
                                        >
                                            <Text style={gstyles.textH2}>จำนวน</Text>
                                            <Text style={gstyles.textH2}>:</Text>
                                        </View>
                                    </View>
                                    <View style={{ flex: 9 }}>
                                        <View style={gstyles.inputContainer}>
                                            <TextInput
                                                keyboardType="numeric"
                                                style={gstyles.inputPrimary}
                                                placeholder="โปรดระบุจำนวนสินค้า"
                                                placeholderTextColor="grey"
                                                onChangeText={stock => {
                                                    this.setState({ stock: stock });
                                                }}
                                            />
                                        </View>
                                    </View>
                                </View>
                                <View style={gstyles.formRowElement}>
                                    <View style={{ flex: 3 }}>
                                        <View
                                            style={{
                                                margin: 5,
                                                alignSelf: 'stretch',
                                                alignItems: 'center',
                                                flexDirection: 'row',
                                                justifyContent: 'space-between',
                                                paddingLeft: 15,
                                                paddingRight: 15,
                                                height: 45,
                                            }}
                                        >
                                            <Text style={gstyles.textH2}>ราคาขาย</Text>
                                            <Text style={gstyles.textH2}>:</Text>
                                        </View>
                                    </View>
                                    <View style={{ flex: 9 }}>
                                        <View style={gstyles.inputContainer}>
                                            <TextInput
                                                keyboardType="numeric"
                                                style={gstyles.inputPrimary}
                                                placeholder="โปรดระบุราคาขาย ( บาท )"
                                                placeholderTextColor="grey"
                                                onChangeText={price => {
                                                    this.setState({ price: price });
                                                }}
                                            />
                                        </View>
                                    </View>
                                </View>
                                <View style={gstyles.formRowElement}>
                                    <View style={{ flex: 3 }}>
                                        <View
                                            style={{
                                                margin: 5,
                                                alignSelf: 'stretch',
                                                alignItems: 'center',
                                                flexDirection: 'row',
                                                justifyContent: 'space-between',
                                                paddingLeft: 15,
                                                paddingRight: 15,
                                                height: 45,
                                            }}
                                        >
                                            <Text style={gstyles.textH2}>ชนิด</Text>
                                            <Text style={gstyles.textH2}>:</Text>
                                        </View>
                                    </View>
                                    <View style={{ flex: 9 }}>
                                        <View style={gstyles.inputContainer}>
                                            <TextInput style={gstyles.inputPrimary} value={type} editable={false} />
                                        </View>
                                    </View>
                                </View>
                                <View style={gstyles.formRowElement}>
                                    <View style={{ flex: 3 }}>
                                        <View
                                            style={{
                                                margin: 5,
                                                alignSelf: 'stretch',
                                                alignItems: 'center',
                                                flexDirection: 'row',
                                                justifyContent: 'space-between',
                                                paddingLeft: 15,
                                                paddingRight: 15,
                                                height: 45,
                                            }}
                                        >
                                            <Text style={gstyles.textH2}>หน่วย</Text>
                                            <Text style={gstyles.textH2}>:</Text>
                                        </View>
                                    </View>
                                    <View style={{ flex: 9 }}>
                                        <View style={gstyles.inputContainer}>
                                            <View style={[gstyles.inputPrimary, { justifyContent: 'center' }]}>
                                                <RNPickerSelect
                                                    placeholder={placeholder}
                                                    value={package_type}
                                                    placeholderTextColor={package_type !== '' ? 'grey' : 'black'}
                                                    color="black"
                                                    onValueChange={value => this.setState({ package_type: value })}
                                                    items={[
                                                        { label: 'กรัม', value: 'กรัม', color: 'black' },
                                                        { label: 'ขีด', value: 'ขีด', color: 'black' },
                                                        { label: 'กิโลกรัม', value: 'กิโลกรัม', color: 'black' },
                                                        { label: 'ถุง', value: 'ถุง', color: 'black' },
                                                        { label: 'มัด', value: 'มัด', color: 'black' },
                                                    ]}
                                                />
                                            </View>
                                        </View>
                                    </View>
                                </View>
                                <View style={gstyles.row}>
                                    <ButtonPrimary name="ยกเลิก" type="primary" onPress={() => this.toggleModal(!modal_visible)} />
                                    <ButtonSecondary name="เพิ่มรายการวัตถุดิบ" type="secondary" onPress={() => this.addNewIngredian()} />
                                </View>
                            </View>
                        </View>
                    </View>
                </Modal>
            </View>
        );
    }
}

const mapStateToProps = state => {
    return {
        order: state.order,
        stock: state.stock,
    };
};

export default connect(mapStateToProps, { addItemToCart, updateStock })(VegetableOrderScreen);

AppRegistry.registerComponent('animatedbasic', () => animatedbasic);
