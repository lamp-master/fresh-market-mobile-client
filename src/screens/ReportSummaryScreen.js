import React, { Component } from 'react';
import { Text, View, FlatList, StyleSheet, ScrollView, TouchableOpacity } from 'react-native';
import { Header, Body, Left, Right, Icon, Title, Tab, Tabs } from 'native-base';
import { NavigationActions } from 'react-navigation';
import { database } from 'firebase/app';
import { styles as gstyles } from '../styles/styles';
import DatePicker from 'react-native-datepicker';

export class ReportSummaryScreen extends Component {
    _isMounted = false;

    state = {
        bills: [],
        ingredians: [],
        vegetable_sum_price: 0,
        seafood_sum_price: 0,
        pork_sum_price: 0,
        chicken_sum_price: 0,
        beef_sum_price: 0,
        other_sum_price: 0,
        date: '',
        dateForChange: '',
        total_price: 0,
    };

    componentWillUnmount() {
        this._isMounted = false;
    }

    componentDidMount() {
        console.disableYellowBox = true;
        this._isMounted = true;
        this.db = database();
        this.fetchDailyBillReport(this.fetchDateFormat());
        this.setState({ date: this.fetchDateFormat(), dateForChange: this.fetchDateFormat() });
    }

    resetState = () => {
        this.setState({
            bills: [],
            ingredians: [],
            vegetable_sum_price: 0,
            seafood_sum_price: 0,
            pork_sum_price: 0,
            chicken_sum_price: 0,
            beef_sum_price: 0,
            other_sum_price: 0,
            total_price: 0,
        });
    };

    fetchDailyBillReport = date => {
        this.resetState();

        let day = date.split('-');
        let days = day[2] + '/' + day[1] + '/' + day[0];

        const BillRef = this.db.ref('bills/' + days);
        BillRef.on('value', snapshot => {
            if (snapshot.val()) {
                const bill_object = snapshot.val();
                const bill_list = Object.keys(bill_object).map(key => ({
                    ...bill_object[key],
                    uid: key,
                }));

                if (this._isMounted) {
                    this.setState({ bills: bill_list });
                    this.calculateTotalPrice();
                    this.calculateIngredianSummary();
                }
            } else {
                if (this._isMounted) this.setState({ bills: [] });
            }
        });
    };

    calculateIngredianSummary = () => {
        let { bills } = this.state;
        let ingredians = [];
        ingredians.vegetable = [];
        ingredians.seafood = [];
        ingredians.pork = [];
        ingredians.chicken = [];
        ingredians.beef = [];
        ingredians.other = [];
        let vegetable_sum_price = 0;
        let seafood_sum_price = 0;
        let pork_sum_price = 0;
        let chicken_sum_price = 0;
        let beef_sum_price = 0;
        let other_sum_price = 0;
        for (let i = 0; i < bills.length; i++) {
            if (bills[i].bill.item.vegetable) {
                for (let a = 0; a < bills[i].bill.item.vegetable.length; a++) {
                    let existed_item = ingredians.vegetable.find(ingredian => ingredian.uid === bills[i].bill.item.vegetable[a].uid);
                    if (existed_item) {
                        ingredians.vegetable = ingredians.vegetable.map(ingredian =>
                            ingredian.uid === bills[i].bill.item.vegetable[a].uid
                                ? { ...ingredian, quantity: ingredian.quantity + bills[i].bill.item.vegetable[a].quantity, sum_price: ingredian.sum_price + bills[i].bill.item.vegetable[a].sum_price }
                                : ingredian,
                        );
                        vegetable_sum_price = vegetable_sum_price + bills[i].bill.item.vegetable[a].sum_price;
                    } else {
                        ingredians.vegetable.push(bills[i].bill.item.vegetable[a]);
                        vegetable_sum_price = vegetable_sum_price + bills[i].bill.item.vegetable[a].sum_price;
                    }
                }
            }
            if (bills[i].bill.item.seafood) {
                for (let a = 0; a < bills[i].bill.item.seafood.length; a++) {
                    let existed_item = ingredians.seafood.find(ingredian => ingredian.uid === bills[i].bill.item.seafood[a].uid);
                    if (existed_item) {
                        ingredians.seafood = ingredians.seafood.map(ingredian =>
                            ingredian.uid === bills[i].bill.item.seafood[a].uid
                                ? { ...ingredian, quantity: ingredian.quantity + bills[i].bill.item.seafood[a].quantity, sum_price: ingredian.sum_price + bills[i].bill.item.seafood[a].sum_price }
                                : ingredian,
                        );
                        seafood_sum_price = seafood_sum_price + bills[i].bill.item.seafood[a].sum_price;
                    } else {
                        ingredians.seafood.push(bills[i].bill.item.seafood[a]);
                        seafood_sum_price = seafood_sum_price + bills[i].bill.item.seafood[a].sum_price;
                    }
                }
            }
            if (bills[i].bill.item.pork) {
                for (let a = 0; a < bills[i].bill.item.pork.length; a++) {
                    let existed_item = ingredians.pork.find(ingredian => ingredian.uid === bills[i].bill.item.pork[a].uid);
                    if (existed_item) {
                        ingredians.pork = ingredians.pork.map(ingredian =>
                            ingredian.uid === bills[i].bill.item.pork[a].uid
                                ? { ...ingredian, quantity: ingredian.quantity + bills[i].bill.item.pork[a].quantity, sum_price: ingredian.sum_price + bills[i].bill.item.pork[a].sum_price }
                                : ingredian,
                        );
                        pork_sum_price = pork_sum_price + bills[i].bill.item.pork[a].sum_price;
                    } else {
                        ingredians.pork.push(bills[i].bill.item.pork[a]);
                        pork_sum_price = pork_sum_price + bills[i].bill.item.pork[a].sum_price;
                    }
                }
            }
            if (bills[i].bill.item.chicken) {
                for (let a = 0; a < bills[i].bill.item.chicken.length; a++) {
                    let existed_item = ingredians.chicken.find(ingredian => ingredian.uid === bills[i].bill.item.chicken[a].uid);
                    if (existed_item) {
                        ingredians.chicken = ingredians.chicken.map(ingredian =>
                            ingredian.uid === bills[i].bill.item.chicken[a].uid
                                ? { ...ingredian, quantity: ingredian.quantity + bills[i].bill.item.chicken[a].quantity, sum_price: ingredian.sum_price + bills[i].bill.item.chicken[a].sum_price }
                                : ingredian,
                        );
                        chicken_sum_price = chicken_sum_price + bills[i].bill.item.chicken[a].sum_price;
                    } else {
                        ingredians.chicken.push(bills[i].bill.item.chicken[a]);
                        chicken_sum_price = chicken_sum_price + bills[i].bill.item.chicken[a].sum_price;
                    }
                }
            }
            if (bills[i].bill.item.beef) {
                for (let a = 0; a < bills[i].bill.item.beef.length; a++) {
                    let existed_item = ingredians.beef.find(ingredian => ingredian.uid === bills[i].bill.item.beef[a].uid);
                    if (existed_item) {
                        ingredians.beef = ingredians.beef.map(ingredian =>
                            ingredian.uid === bills[i].bill.item.beef[a].uid
                                ? { ...ingredian, quantity: ingredian.quantity + bills[i].bill.item.beef[a].quantity, sum_price: ingredian.sum_price + bills[i].bill.item.beef[a].sum_price }
                                : ingredian,
                        );
                        beef_sum_price = beef_sum_price + bills[i].bill.item.beef[a].sum_price;
                    } else {
                        ingredians.beef.push(bills[i].bill.item.beef[a]);
                        beef_sum_price = beef_sum_price + bills[i].bill.item.beef[a].sum_price;
                    }
                }
            }
            if (bills[i].bill.item.other) {
                for (let a = 0; a < bills[i].bill.item.other.length; a++) {
                    let existed_item = ingredians.other.find(ingredian => ingredian.uid === bills[i].bill.item.other[a].uid);
                    if (existed_item) {
                        ingredians.other = ingredians.other.map(ingredian =>
                            ingredian.uid === bills[i].bill.item.other[a].uid
                                ? { ...ingredian, quantity: ingredian.quantity + bills[i].bill.item.other[a].quantity, sum_price: ingredian.sum_price + bills[i].bill.item.other[a].sum_price }
                                : ingredian,
                        );
                        other_sum_price = other_sum_price + bills[i].bill.item.other[a].sum_price;
                    } else {
                        ingredians.other.push(bills[i].bill.item.other[a]);
                        other_sum_price = other_sum_price + bills[i].bill.item.other[a].sum_price;
                    }
                }
            }
        }
        this.setState({
            ingredians: ingredians,
            vegetable_sum_price: vegetable_sum_price,
            seafood_sum_price: seafood_sum_price,
            pork_sum_price: pork_sum_price,
            chicken_sum_price: chicken_sum_price,
            beef_sum_price: beef_sum_price,
            other_sum_price: other_sum_price,
        });
    };

    calculateTotalPrice = () => {
        let { bills } = this.state;
        let total_price = 0;
        for (var i = 0; i < bills.length; i++) {
            total_price = total_price + bills[i].bill.total_price;
        }
        this.setState({ total_price: total_price });
    };

    Date = () => {
        var date = new Date().getDate();
        if (date < 10) date = '0' + date;
        var month = new Date().getMonth() + 1;
        if (month < 10) month = '0' + month;
        var year = new Date().getFullYear();
        let DateStamp = year + '/' + month + '/' + date;
        return DateStamp;
    };

    fetchDateFormat = () => {
        var date = new Date().getDate();
        if (date < 10) date = '0' + date;
        var month = new Date().getMonth() + 1;
        if (month < 10) month = '0' + month;
        var year = new Date().getFullYear();
        let DateStamp = date + '-' + month + '-' + year;
        return DateStamp;
    };

    render() {
        let { navigation } = this.props;
        let { bills, ingredians, date, dateForChange, total_price, vegetable_sum_price, seafood_sum_price, pork_sum_price, chicken_sum_price, other_sum_price, beef_sum_price } = this.state;
        return (
            <View style={{ flex: 1, backgroundColor: 'lightgrey' }}>
                <Header>
                    <Left>
                        <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', paddingLeft: '1%' }} onPress={() => navigation.dispatch(NavigationActions.back())}>
                            <Icon type="FontAwesome" name="arrow-left" />
                            <Title style={{ paddingLeft: 10 }}>ย้อนกลับ</Title>
                        </TouchableOpacity>
                    </Left>
                    <Body style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                        <Title style={{ paddingRight: 10 }}>บิลประจำวันที่ </Title>
                        <DatePicker
                            style={{ width: 200 }}
                            date={dateForChange}
                            mode="date"
                            placeholder="select date"
                            format="DD-MM-YYYY"
                            minDate="01-01-2019"
                            maxDate={date}
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            customStyles={{
                                dateIcon: {
                                    position: 'absolute',
                                    right: 0,
                                    top: 4,
                                    marginRight: 0,
                                },
                                dateInput: {
                                    marginRight: 36,
                                },
                            }}
                            onDateChange={date => {
                                this.fetchDailyBillReport(date);
                                this.setState({ dateForChange: date });
                            }}
                        />
                    </Body>
                    <Right style={{ flexDirection: 'row', alignItems: 'center', paddingRight: '1%' }}>
                        <Text style={gstyles.textH2}>
                            รายได้ของวันนี้{' '}
                            {parseFloat(total_price)
                                .toFixed(2)
                                .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}{' '}
                            บาท
                        </Text>
                    </Right>
                </Header>

                <Tabs>
                    {/* SORT BY BILL */}
                    <Tab heading="กรองตามบิล">
                        <ScrollView>
                            <FlatList
                                data={bills}
                                contentContainerStyle={{ flex: 1, alignItems: 'stretch', marginTop: '1%' }}
                                listKey={(item, index) => 'D' + index.toString()}
                                keyExtractor={item => item.uid}
                                renderItem={({ item, index }) => (
                                    <View
                                        style={{
                                            width: '95%',
                                            alignItems: 'center',
                                            alignSelf: 'center',
                                            borderWidth: 1,
                                            padding: '1%',
                                            marginTop: '1%',
                                            borderColor: 'black',
                                            borderRadius: 5,
                                            backgroundColor: 'white',
                                        }}
                                    >
                                        <View
                                            style={{
                                                flexDirection: 'row',
                                                width: '95%',
                                                borderBottomWidth: 2,
                                                borderBottomColor: 'black',
                                                alignItems: 'center',
                                                alignSelf: 'center',
                                            }}
                                        >
                                            <View style={{ width: '40%' }}>
                                                <Text style={gstyles.textH1}>{item.time}</Text>
                                            </View>
                                            <View style={{ width: '20%', alignItems: 'flex-end' }}>
                                                <Text style={gstyles.textH1}>จำนวน</Text>
                                            </View>
                                            <View style={{ width: '20%', alignItems: 'flex-end' }}>
                                                <Text style={gstyles.textH1}>ราคาต่อหน่วย</Text>
                                            </View>
                                            <View style={{ width: '20%', alignItems: 'flex-end' }}>
                                                <Text style={gstyles.textH1}>ราคา</Text>
                                            </View>
                                        </View>
                                        <View>
                                            {item.bill.item.vegetable ? (
                                                <View style={{ width: '95%', marginTop: '1%', alignItems: 'center', alignSelf: 'center', borderBottomWidth: 1, borderBottomColor: 'black' }}>
                                                    <View style={{ width: '100%', alignItems: 'flex-start', alignSelf: 'flex-start' }}>
                                                        <Text style={gstyles.textH1}>ผัก</Text>
                                                    </View>
                                                    <FlatList
                                                        data={item.bill.item.vegetable}
                                                        keyExtractor={item => item.uid}
                                                        listKey={(item, index) => 'D2' + index.toString()}
                                                        renderItem={({ item, index }) => (
                                                            <View style={{ flexDirection: 'row', width: '100%', alignItems: 'center', alignSelf: 'center' }}>
                                                                <View style={{ width: '40%' }}>
                                                                    <Text style={gstyles.textH3}>{item.name}</Text>
                                                                </View>
                                                                <View style={{ width: '20%', alignItems: 'flex-end' }}>
                                                                    <Text style={gstyles.textH3}>{item.quantity}</Text>
                                                                </View>
                                                                <View style={{ width: '20%', alignItems: 'flex-end' }}>
                                                                    <Text style={gstyles.textH3}>{item.price}</Text>
                                                                </View>
                                                                <View style={{ width: '20%', alignItems: 'flex-end' }}>
                                                                    <Text style={gstyles.textH3}>
                                                                        {parseFloat(item.sum_price)
                                                                            .toFixed(2)
                                                                            .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}
                                                                    </Text>
                                                                </View>
                                                            </View>
                                                        )}
                                                    />
                                                </View>
                                            ) : (
                                                <View></View>
                                            )}
                                        </View>
                                        <View>
                                            {item.bill.item.seafood ? (
                                                <View style={{ width: '95%', marginTop: '1%', alignItems: 'center', alignSelf: 'center', borderBottomWidth: 1, borderBottomColor: 'black' }}>
                                                    <View style={{ width: '100%', alignItems: 'flex-start', alignSelf: 'flex-start' }}>
                                                        <Text style={gstyles.textH1}>ซีฟู๊ด</Text>
                                                    </View>
                                                    <FlatList
                                                        data={item.bill.item.seafood}
                                                        keyExtractor={item => item.uid}
                                                        listKey={(item, index) => 'D3' + index.toString()}
                                                        renderItem={({ item, index }) => (
                                                            <View style={{ flexDirection: 'row', width: '100%', alignItems: 'center', alignSelf: 'center' }}>
                                                                <View style={{ width: '40%' }}>
                                                                    <Text style={gstyles.textH3}>{item.name}</Text>
                                                                </View>
                                                                <View style={{ width: '20%', alignItems: 'flex-end' }}>
                                                                    <Text style={gstyles.textH3}>{item.quantity}</Text>
                                                                </View>
                                                                <View style={{ width: '20%', alignItems: 'flex-end' }}>
                                                                    <Text style={gstyles.textH3}>{item.price}</Text>
                                                                </View>
                                                                <View style={{ width: '20%', alignItems: 'flex-end' }}>
                                                                    <Text style={gstyles.textH3}>
                                                                        {parseFloat(item.sum_price)
                                                                            .toFixed(2)
                                                                            .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}
                                                                    </Text>
                                                                </View>
                                                            </View>
                                                        )}
                                                    />
                                                </View>
                                            ) : (
                                                <View></View>
                                            )}
                                        </View>
                                        <View>
                                            {item.bill.item.pork ? (
                                                <View style={{ width: '95%', marginTop: '1%', alignItems: 'center', alignSelf: 'center', borderBottomWidth: 1, borderBottomColor: 'black' }}>
                                                    <View style={{ width: '100%', alignItems: 'flex-start', alignSelf: 'flex-start' }}>
                                                        <Text style={gstyles.textH1}>หมู</Text>
                                                    </View>
                                                    <FlatList
                                                        data={item.bill.item.pork}
                                                        keyExtractor={item => item.uid}
                                                        listKey={(item, index) => 'D4' + index.toString()}
                                                        renderItem={({ item, index }) => (
                                                            <View style={{ flexDirection: 'row', width: '100%', alignItems: 'center', alignSelf: 'center' }}>
                                                                <View style={{ width: '40%' }}>
                                                                    <Text style={gstyles.textH3}>{item.name}</Text>
                                                                </View>
                                                                <View style={{ width: '20%', alignItems: 'flex-end' }}>
                                                                    <Text style={gstyles.textH3}>{item.quantity}</Text>
                                                                </View>
                                                                <View style={{ width: '20%', alignItems: 'flex-end' }}>
                                                                    <Text style={gstyles.textH3}>{item.price}</Text>
                                                                </View>
                                                                <View style={{ width: '20%', alignItems: 'flex-end' }}>
                                                                    <Text style={gstyles.textH3}>
                                                                        {parseFloat(item.sum_price)
                                                                            .toFixed(2)
                                                                            .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}
                                                                    </Text>
                                                                </View>
                                                            </View>
                                                        )}
                                                    />
                                                </View>
                                            ) : (
                                                <View></View>
                                            )}
                                        </View>
                                        <View>
                                            {item.bill.item.chicken ? (
                                                <View style={{ width: '95%', marginTop: '1%', alignItems: 'center', alignSelf: 'center', borderBottomWidth: 1, borderBottomColor: 'black' }}>
                                                    <View style={{ width: '100%', alignItems: 'flex-start', alignSelf: 'flex-start' }}>
                                                        <Text style={gstyles.textH1}>ไก่</Text>
                                                    </View>
                                                    <FlatList
                                                        data={item.bill.item.chicken}
                                                        keyExtractor={item => item.uid}
                                                        listKey={(item, index) => 'D5' + index.toString()}
                                                        renderItem={({ item, index }) => (
                                                            <View style={{ flexDirection: 'row', width: '100%', alignItems: 'center', alignSelf: 'center' }}>
                                                                <View style={{ width: '40%' }}>
                                                                    <Text style={gstyles.textH3}>{item.name}</Text>
                                                                </View>
                                                                <View style={{ width: '20%', alignItems: 'flex-end' }}>
                                                                    <Text style={gstyles.textH3}>{item.quantity}</Text>
                                                                </View>
                                                                <View style={{ width: '20%', alignItems: 'flex-end' }}>
                                                                    <Text style={gstyles.textH3}>{item.price}</Text>
                                                                </View>
                                                                <View style={{ width: '20%', alignItems: 'flex-end' }}>
                                                                    <Text style={gstyles.textH3}>
                                                                        {parseFloat(item.sum_price)
                                                                            .toFixed(2)
                                                                            .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}
                                                                    </Text>
                                                                </View>
                                                            </View>
                                                        )}
                                                    />
                                                </View>
                                            ) : (
                                                <View></View>
                                            )}
                                        </View>
                                        <View>
                                            {item.bill.item.beef ? (
                                                <View style={{ width: '95%', marginTop: '1%', alignItems: 'center', alignSelf: 'center', borderBottomWidth: 1, borderBottomColor: 'black' }}>
                                                    <View style={{ width: '100%', alignItems: 'flex-start', alignSelf: 'flex-start' }}>
                                                        <Text style={gstyles.textH1}>เนื้อ</Text>
                                                    </View>
                                                    <FlatList
                                                        data={item.bill.item.beef}
                                                        keyExtractor={item => item.uid}
                                                        listKey={(item, index) => 'D6' + index.toString()}
                                                        renderItem={({ item, index }) => (
                                                            <View style={{ flexDirection: 'row', width: '100%', alignItems: 'center', alignSelf: 'center' }}>
                                                                <View style={{ width: '40%' }}>
                                                                    <Text style={gstyles.textH3}>{item.name}</Text>
                                                                </View>
                                                                <View style={{ width: '20%', alignItems: 'flex-end' }}>
                                                                    <Text style={gstyles.textH3}>{item.quantity}</Text>
                                                                </View>
                                                                <View style={{ width: '20%', alignItems: 'flex-end' }}>
                                                                    <Text style={gstyles.textH3}>{item.price}</Text>
                                                                </View>
                                                                <View style={{ width: '20%', alignItems: 'flex-end' }}>
                                                                    <Text style={gstyles.textH3}>
                                                                        {parseFloat(item.sum_price)
                                                                            .toFixed(2)
                                                                            .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}
                                                                    </Text>
                                                                </View>
                                                            </View>
                                                        )}
                                                    />
                                                </View>
                                            ) : (
                                                <View></View>
                                            )}
                                        </View>
                                        <View>
                                            {item.bill.item.other ? (
                                                <View style={{ width: '95%', marginTop: '1%', alignItems: 'center', alignSelf: 'center', borderBottomWidth: 1, borderBottomColor: 'black' }}>
                                                    <View style={{ width: '100%', alignItems: 'flex-start', alignSelf: 'flex-start' }}>
                                                        <Text style={gstyles.textH1}>อื่นๆ</Text>
                                                    </View>
                                                    <FlatList
                                                        data={item.bill.item.other}
                                                        keyExtractor={item => item.uid}
                                                        listKey={(item, index) => 'D7' + index.toString()}
                                                        renderItem={({ item, index }) => (
                                                            <View style={{ flexDirection: 'row', width: '100%', alignItems: 'center', alignSelf: 'center' }}>
                                                                <View style={{ width: '40%' }}>
                                                                    <Text style={gstyles.textH3}>{item.name}</Text>
                                                                </View>
                                                                <View style={{ width: '20%', alignItems: 'flex-end' }}>
                                                                    <Text style={gstyles.textH3}>{item.quantity}</Text>
                                                                </View>
                                                                <View style={{ width: '20%', alignItems: 'flex-end' }}>
                                                                    <Text style={gstyles.textH3}>{item.price}</Text>
                                                                </View>
                                                                <View style={{ width: '20%', alignItems: 'flex-end' }}>
                                                                    <Text style={gstyles.textH3}>
                                                                        {parseFloat(item.sum_price)
                                                                            .toFixed(2)
                                                                            .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}
                                                                    </Text>
                                                                </View>
                                                            </View>
                                                        )}
                                                    />
                                                </View>
                                            ) : (
                                                <View></View>
                                            )}
                                        </View>
                                        <View
                                            style={{
                                                flexDirection: 'row',
                                                width: '95%',
                                                alignItems: 'center',
                                                alignSelf: 'center',
                                            }}
                                        >
                                            <View style={{ width: '50%' }}>
                                                <Text style={gstyles.textH1}>ค่าจัดส่ง</Text>
                                            </View>

                                            <View style={{ width: '50%', alignItems: 'flex-end' }}>
                                                <Text style={gstyles.textH1}>
                                                    {parseFloat(item.bill.distance.price)
                                                        .toFixed(2)
                                                        .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}{' '}
                                                    บาท
                                                </Text>
                                            </View>
                                        </View>
                                        <View
                                            style={{
                                                flexDirection: 'row',
                                                width: '95%',
                                                alignItems: 'center',
                                                alignSelf: 'center',
                                            }}
                                        >
                                            <View style={{ width: '50%' }}>
                                                <Text style={gstyles.textH1}>รวมยอดทั้งสิ้น</Text>
                                            </View>

                                            <View style={{ width: '50%', alignItems: 'flex-end' }}>
                                                <Text style={gstyles.textH1}>
                                                    {parseFloat(item.bill.total_price)
                                                        .toFixed(2)
                                                        .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}{' '}
                                                    บาท
                                                </Text>
                                            </View>
                                        </View>
                                    </View>
                                )}
                            />
                        </ScrollView>
                    </Tab>
                    <Tab heading="กรองตามชนิดอาหาร">
                        <ScrollView>
                            {ingredians.vegetable ? (
                                <View
                                    style={{
                                        width: '95%',
                                        alignItems: 'stretch',
                                        alignSelf: 'center',
                                        borderWidth: 1,
                                        padding: '1%',
                                        marginTop: '2%',
                                        borderColor: 'black',
                                        borderRadius: 5,
                                        backgroundColor: 'white',
                                    }}
                                >
                                    <View
                                        style={{
                                            flexDirection: 'row',
                                            width: '95%',
                                            borderBottomWidth: 2,
                                            borderBottomColor: 'black',
                                            alignItems: 'center',
                                            alignSelf: 'center',
                                        }}
                                    >
                                        <View style={{ width: '40%' }}>
                                            <Text style={gstyles.textTitle}>ชนิดรายการ : ผัก</Text>
                                        </View>
                                        <View style={{ width: '20%', alignItems: 'flex-end' }}>
                                            <Text style={gstyles.textH1}>จำนวน</Text>
                                        </View>
                                        <View style={{ width: '20%', alignItems: 'flex-end' }}>
                                            <Text style={gstyles.textH1}>ราคาต่อหน่วย</Text>
                                        </View>
                                        <View style={{ width: '20%', alignItems: 'flex-end' }}>
                                            <Text style={gstyles.textH1}>ราคา</Text>
                                        </View>
                                    </View>
                                    <View>
                                        <FlatList
                                            data={ingredians.vegetable}
                                            keyExtractor={item => item.uid}
                                            listKey={(item, index) => 'D8' + index.toString()}
                                            renderItem={({ item, index }) => (
                                                <View
                                                    style={{
                                                        flexDirection: 'row',
                                                        width: '95%',
                                                        alignItems: 'center',
                                                        alignSelf: 'center',
                                                    }}
                                                >
                                                    <View style={{ width: '40%' }}>
                                                        <Text style={gstyles.textH2}>{item.name}</Text>
                                                    </View>
                                                    <View style={{ width: '20%', alignItems: 'flex-end' }}>
                                                        <Text style={gstyles.textH2}>{item.quantity}</Text>
                                                    </View>
                                                    <View style={{ width: '20%', alignItems: 'flex-end' }}>
                                                        <Text style={gstyles.textH2}>
                                                            {parseFloat(item.price)
                                                                .toFixed(2)
                                                                .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}
                                                        </Text>
                                                    </View>
                                                    <View style={{ width: '20%', alignItems: 'flex-end' }}>
                                                        <Text style={gstyles.textH2}>
                                                            {parseFloat(item.sum_price)
                                                                .toFixed(2)
                                                                .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}{' '}
                                                            บาท
                                                        </Text>
                                                    </View>
                                                </View>
                                            )}
                                        />
                                        <View
                                            style={{
                                                flexDirection: 'row',
                                                width: '95%',
                                                alignItems: 'center',
                                                alignSelf: 'center',
                                                borderTopWidth: 2,
                                                borderTopColor: 'black',
                                            }}
                                        >
                                            <View style={{ width: '50%' }}>
                                                <Text style={gstyles.textH1}>รวมยอดทั้งสิ้น</Text>
                                            </View>

                                            <View style={{ width: '50%', alignItems: 'flex-end' }}>
                                                <Text style={gstyles.textH1}>
                                                    {parseFloat(vegetable_sum_price)
                                                        .toFixed(2)
                                                        .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}{' '}
                                                    บาท
                                                </Text>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                            ) : (
                                <View></View>
                            )}

                            {ingredians.seafood ? (
                                <View
                                    style={{
                                        width: '95%',
                                        alignItems: 'stretch',
                                        alignSelf: 'center',
                                        borderWidth: 1,
                                        padding: '1%',
                                        marginTop: '2%',
                                        borderColor: 'black',
                                        borderRadius: 5,
                                        backgroundColor: 'white',
                                    }}
                                >
                                    <View
                                        style={{
                                            flexDirection: 'row',
                                            width: '95%',
                                            borderBottomWidth: 2,
                                            borderBottomColor: 'black',
                                            alignItems: 'center',
                                            alignSelf: 'center',
                                        }}
                                    >
                                        <View style={{ width: '40%' }}>
                                            <Text style={gstyles.textTitle}>ชนิดรายการ : อาหารทะเล</Text>
                                        </View>
                                        <View style={{ width: '20%', alignItems: 'flex-end' }}>
                                            <Text style={gstyles.textH1}>จำนวน</Text>
                                        </View>
                                        <View style={{ width: '20%', alignItems: 'flex-end' }}>
                                            <Text style={gstyles.textH1}>ราคาต่อหน่วย</Text>
                                        </View>
                                        <View style={{ width: '20%', alignItems: 'flex-end' }}>
                                            <Text style={gstyles.textH1}>ราคา</Text>
                                        </View>
                                    </View>
                                    <View>
                                        <FlatList
                                            data={ingredians.seafood}
                                            keyExtractor={item => item.uid}
                                            listKey={(item, index) => 'D8' + index.toString()}
                                            renderItem={({ item, index }) => (
                                                <View
                                                    style={{
                                                        flexDirection: 'row',
                                                        width: '95%',
                                                        alignItems: 'center',
                                                        alignSelf: 'center',
                                                    }}
                                                >
                                                    <View style={{ width: '40%' }}>
                                                        <Text style={gstyles.textH2}>{item.name}</Text>
                                                    </View>
                                                    <View style={{ width: '20%', alignItems: 'flex-end' }}>
                                                        <Text style={gstyles.textH2}>{item.quantity}</Text>
                                                    </View>
                                                    <View style={{ width: '20%', alignItems: 'flex-end' }}>
                                                        <Text style={gstyles.textH2}>
                                                            {parseFloat(item.price)
                                                                .toFixed(2)
                                                                .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}
                                                        </Text>
                                                    </View>
                                                    <View style={{ width: '20%', alignItems: 'flex-end' }}>
                                                        <Text style={gstyles.textH2}>
                                                            {parseFloat(item.sum_price)
                                                                .toFixed(2)
                                                                .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}{' '}
                                                            บาท
                                                        </Text>
                                                    </View>
                                                </View>
                                            )}
                                        />
                                        <View
                                            style={{
                                                flexDirection: 'row',
                                                width: '95%',
                                                alignItems: 'center',
                                                alignSelf: 'center',
                                                borderTopWidth: 2,
                                                borderTopColor: 'black',
                                            }}
                                        >
                                            <View style={{ width: '50%' }}>
                                                <Text style={gstyles.textH1}>รวมยอดทั้งสิ้น</Text>
                                            </View>

                                            <View style={{ width: '50%', alignItems: 'flex-end' }}>
                                                <Text style={gstyles.textH1}>
                                                    {parseFloat(seafood_sum_price)
                                                        .toFixed(2)
                                                        .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}{' '}
                                                    บาท
                                                </Text>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                            ) : (
                                <View></View>
                            )}

                            {ingredians.pork ? (
                                <View
                                    style={{
                                        width: '95%',
                                        alignItems: 'stretch',
                                        alignSelf: 'center',
                                        borderWidth: 1,
                                        padding: '1%',
                                        marginTop: '2%',
                                        borderColor: 'black',
                                        borderRadius: 5,
                                        backgroundColor: 'white',
                                    }}
                                >
                                    <View
                                        style={{
                                            flexDirection: 'row',
                                            width: '95%',
                                            borderBottomWidth: 2,
                                            borderBottomColor: 'black',
                                            alignItems: 'center',
                                            alignSelf: 'center',
                                        }}
                                    >
                                        <View style={{ width: '40%' }}>
                                            <Text style={gstyles.textTitle}>ชนิดรายการ : เนื้อหมู</Text>
                                        </View>
                                        <View style={{ width: '20%', alignItems: 'flex-end' }}>
                                            <Text style={gstyles.textH1}>จำนวน</Text>
                                        </View>
                                        <View style={{ width: '20%', alignItems: 'flex-end' }}>
                                            <Text style={gstyles.textH1}>ราคาต่อหน่วย</Text>
                                        </View>
                                        <View style={{ width: '20%', alignItems: 'flex-end' }}>
                                            <Text style={gstyles.textH1}>ราคา</Text>
                                        </View>
                                    </View>
                                    <View>
                                        <FlatList
                                            data={ingredians.pork}
                                            keyExtractor={item => item.uid}
                                            listKey={(item, index) => 'D8' + index.toString()}
                                            renderItem={({ item, index }) => (
                                                <View
                                                    style={{
                                                        flexDirection: 'row',
                                                        width: '95%',
                                                        alignItems: 'center',
                                                        alignSelf: 'center',
                                                    }}
                                                >
                                                    <View style={{ width: '40%' }}>
                                                        <Text style={gstyles.textH2}>{item.name}</Text>
                                                    </View>
                                                    <View style={{ width: '20%', alignItems: 'flex-end' }}>
                                                        <Text style={gstyles.textH2}>{item.quantity}</Text>
                                                    </View>
                                                    <View style={{ width: '20%', alignItems: 'flex-end' }}>
                                                        <Text style={gstyles.textH2}>
                                                            {parseFloat(item.price)
                                                                .toFixed(2)
                                                                .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}
                                                        </Text>
                                                    </View>
                                                    <View style={{ width: '20%', alignItems: 'flex-end' }}>
                                                        <Text style={gstyles.textH2}>
                                                            {parseFloat(item.sum_price)
                                                                .toFixed(2)
                                                                .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}{' '}
                                                            บาท
                                                        </Text>
                                                    </View>
                                                </View>
                                            )}
                                        />
                                        <View
                                            style={{
                                                flexDirection: 'row',
                                                width: '95%',
                                                alignItems: 'center',
                                                alignSelf: 'center',
                                                borderTopWidth: 2,
                                                borderTopColor: 'black',
                                            }}
                                        >
                                            <View style={{ width: '50%' }}>
                                                <Text style={gstyles.textH1}>รวมยอดทั้งสิ้น</Text>
                                            </View>

                                            <View style={{ width: '50%', alignItems: 'flex-end' }}>
                                                <Text style={gstyles.textH1}>
                                                    {parseFloat(pork_sum_price)
                                                        .toFixed(2)
                                                        .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}{' '}
                                                    บาท
                                                </Text>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                            ) : (
                                <View></View>
                            )}

                            {ingredians.chicken ? (
                                <View
                                    style={{
                                        width: '95%',
                                        alignItems: 'stretch',
                                        alignSelf: 'center',
                                        borderWidth: 1,
                                        padding: '1%',
                                        marginTop: '2%',
                                        borderColor: 'black',
                                        borderRadius: 5,
                                        backgroundColor: 'white',
                                    }}
                                >
                                    <View
                                        style={{
                                            flexDirection: 'row',
                                            width: '95%',
                                            borderBottomWidth: 2,
                                            borderBottomColor: 'black',
                                            alignItems: 'center',
                                            alignSelf: 'center',
                                        }}
                                    >
                                        <View style={{ width: '40%' }}>
                                            <Text style={gstyles.textTitle}>ชนิดรายการ : เนื้อไก่</Text>
                                        </View>
                                        <View style={{ width: '20%', alignItems: 'flex-end' }}>
                                            <Text style={gstyles.textH1}>จำนวน</Text>
                                        </View>
                                        <View style={{ width: '20%', alignItems: 'flex-end' }}>
                                            <Text style={gstyles.textH1}>ราคาต่อหน่วย</Text>
                                        </View>
                                        <View style={{ width: '20%', alignItems: 'flex-end' }}>
                                            <Text style={gstyles.textH1}>ราคา</Text>
                                        </View>
                                    </View>
                                    <View>
                                        <FlatList
                                            data={ingredians.chicken}
                                            keyExtractor={item => item.uid}
                                            listKey={(item, index) => 'D8' + index.toString()}
                                            renderItem={({ item, index }) => (
                                                <View
                                                    style={{
                                                        flexDirection: 'row',
                                                        width: '95%',
                                                        alignItems: 'center',
                                                        alignSelf: 'center',
                                                    }}
                                                >
                                                    <View style={{ width: '40%' }}>
                                                        <Text style={gstyles.textH2}>{item.name}</Text>
                                                    </View>
                                                    <View style={{ width: '20%', alignItems: 'flex-end' }}>
                                                        <Text style={gstyles.textH2}>{item.quantity}</Text>
                                                    </View>
                                                    <View style={{ width: '20%', alignItems: 'flex-end' }}>
                                                        <Text style={gstyles.textH2}>
                                                            {parseFloat(item.price)
                                                                .toFixed(2)
                                                                .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}
                                                        </Text>
                                                    </View>
                                                    <View style={{ width: '20%', alignItems: 'flex-end' }}>
                                                        <Text style={gstyles.textH2}>
                                                            {parseFloat(item.sum_price)
                                                                .toFixed(2)
                                                                .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}{' '}
                                                            บาท
                                                        </Text>
                                                    </View>
                                                </View>
                                            )}
                                        />
                                        <View
                                            style={{
                                                flexDirection: 'row',
                                                width: '95%',
                                                alignItems: 'center',
                                                alignSelf: 'center',
                                                borderTopWidth: 2,
                                                borderTopColor: 'black',
                                            }}
                                        >
                                            <View style={{ width: '50%' }}>
                                                <Text style={gstyles.textH1}>รวมยอดทั้งสิ้น</Text>
                                            </View>

                                            <View style={{ width: '50%', alignItems: 'flex-end' }}>
                                                <Text style={gstyles.textH1}>
                                                    {parseFloat(chicken_sum_price)
                                                        .toFixed(2)
                                                        .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}{' '}
                                                    บาท
                                                </Text>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                            ) : (
                                <View></View>
                            )}
                            {ingredians.beef ? (
                                <View
                                    style={{
                                        width: '95%',
                                        alignItems: 'stretch',
                                        alignSelf: 'center',
                                        borderWidth: 1,
                                        padding: '1%',
                                        marginTop: '2%',
                                        borderColor: 'black',
                                        borderRadius: 5,
                                        backgroundColor: 'white',
                                    }}
                                >
                                    <View
                                        style={{
                                            flexDirection: 'row',
                                            width: '95%',
                                            borderBottomWidth: 2,
                                            borderBottomColor: 'black',
                                            alignItems: 'center',
                                            alignSelf: 'center',
                                        }}
                                    >
                                        <View style={{ width: '40%' }}>
                                            <Text style={gstyles.textTitle}>ชนิดรายการ : เนื้อ</Text>
                                        </View>
                                        <View style={{ width: '20%', alignItems: 'flex-end' }}>
                                            <Text style={gstyles.textH1}>จำนวน</Text>
                                        </View>
                                        <View style={{ width: '20%', alignItems: 'flex-end' }}>
                                            <Text style={gstyles.textH1}>ราคาต่อหน่วย</Text>
                                        </View>
                                        <View style={{ width: '20%', alignItems: 'flex-end' }}>
                                            <Text style={gstyles.textH1}>ราคา</Text>
                                        </View>
                                    </View>
                                    <View>
                                        <FlatList
                                            data={ingredians.beef}
                                            keyExtractor={item => item.uid}
                                            listKey={(item, index) => 'D8' + index.toString()}
                                            renderItem={({ item, index }) => (
                                                <View
                                                    style={{
                                                        flexDirection: 'row',
                                                        width: '95%',
                                                        alignItems: 'center',
                                                        alignSelf: 'center',
                                                    }}
                                                >
                                                    <View style={{ width: '40%' }}>
                                                        <Text style={gstyles.textH2}>{item.name}</Text>
                                                    </View>
                                                    <View style={{ width: '20%', alignItems: 'flex-end' }}>
                                                        <Text style={gstyles.textH2}>{item.quantity}</Text>
                                                    </View>
                                                    <View style={{ width: '20%', alignItems: 'flex-end' }}>
                                                        <Text style={gstyles.textH2}>
                                                            {parseFloat(item.price)
                                                                .toFixed(2)
                                                                .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}
                                                        </Text>
                                                    </View>
                                                    <View style={{ width: '20%', alignItems: 'flex-end' }}>
                                                        <Text style={gstyles.textH2}>
                                                            {parseFloat(item.sum_price)
                                                                .toFixed(2)
                                                                .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}{' '}
                                                            บาท
                                                        </Text>
                                                    </View>
                                                </View>
                                            )}
                                        />
                                        <View
                                            style={{
                                                flexDirection: 'row',
                                                width: '95%',
                                                alignItems: 'center',
                                                alignSelf: 'center',
                                                borderTopWidth: 2,
                                                borderTopColor: 'black',
                                            }}
                                        >
                                            <View style={{ width: '50%' }}>
                                                <Text style={gstyles.textH1}>รวมยอดทั้งสิ้น</Text>
                                            </View>

                                            <View style={{ width: '50%', alignItems: 'flex-end' }}>
                                                <Text style={gstyles.textH1}>
                                                    {parseFloat(beef_sum_price)
                                                        .toFixed(2)
                                                        .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}{' '}
                                                    บาท
                                                </Text>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                            ) : (
                                <View></View>
                            )}
                            {ingredians.other ? (
                                <View
                                    style={{
                                        width: '95%',
                                        alignItems: 'stretch',
                                        alignSelf: 'center',
                                        borderWidth: 1,
                                        padding: '1%',
                                        marginTop: '2%',
                                        marginBottom: '2%',
                                        borderColor: 'black',
                                        borderRadius: 5,
                                        backgroundColor: 'white',
                                    }}
                                >
                                    <View
                                        style={{
                                            flexDirection: 'row',
                                            width: '95%',
                                            borderBottomWidth: 2,
                                            borderBottomColor: 'black',
                                            alignItems: 'center',
                                            alignSelf: 'center',
                                        }}
                                    >
                                        <View style={{ width: '40%' }}>
                                            <Text style={gstyles.textTitle}>ชนิดรายการ : อื่นๆ</Text>
                                        </View>
                                        <View style={{ width: '20%', alignItems: 'flex-end' }}>
                                            <Text style={gstyles.textH1}>จำนวน</Text>
                                        </View>
                                        <View style={{ width: '20%', alignItems: 'flex-end' }}>
                                            <Text style={gstyles.textH1}>ราคาต่อหน่วย</Text>
                                        </View>
                                        <View style={{ width: '20%', alignItems: 'flex-end' }}>
                                            <Text style={gstyles.textH1}>ราคา</Text>
                                        </View>
                                    </View>
                                    <View>
                                        <FlatList
                                            data={ingredians.other}
                                            keyExtractor={item => item.uid}
                                            listKey={(item, index) => 'D8' + index.toString()}
                                            renderItem={({ item, index }) => (
                                                <View
                                                    style={{
                                                        flexDirection: 'row',
                                                        width: '95%',
                                                        alignItems: 'center',
                                                        alignSelf: 'center',
                                                    }}
                                                >
                                                    <View style={{ width: '40%' }}>
                                                        <Text style={gstyles.textH2}>{item.name}</Text>
                                                    </View>
                                                    <View style={{ width: '20%', alignItems: 'flex-end' }}>
                                                        <Text style={gstyles.textH2}>{item.quantity}</Text>
                                                    </View>
                                                    <View style={{ width: '20%', alignItems: 'flex-end' }}>
                                                        <Text style={gstyles.textH2}>
                                                            {parseFloat(item.price)
                                                                .toFixed(2)
                                                                .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}
                                                        </Text>
                                                    </View>
                                                    <View style={{ width: '20%', alignItems: 'flex-end' }}>
                                                        <Text style={gstyles.textH2}>
                                                            {parseFloat(item.sum_price)
                                                                .toFixed(2)
                                                                .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}{' '}
                                                            บาท
                                                        </Text>
                                                    </View>
                                                </View>
                                            )}
                                        />
                                        <View
                                            style={{
                                                flexDirection: 'row',
                                                width: '95%',
                                                alignItems: 'center',
                                                alignSelf: 'center',
                                                borderTopWidth: 2,
                                                borderTopColor: 'black',
                                            }}
                                        >
                                            <View style={{ width: '50%' }}>
                                                <Text style={gstyles.textH1}>รวมยอดทั้งสิ้น</Text>
                                            </View>

                                            <View style={{ width: '50%', alignItems: 'flex-end' }}>
                                                <Text style={gstyles.textH1}>
                                                    {parseFloat(other_sum_price)
                                                        .toFixed(2)
                                                        .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}{' '}
                                                    บาท
                                                </Text>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                            ) : (
                                <View></View>
                            )}
                        </ScrollView>
                    </Tab>
                </Tabs>
            </View>
        );
    }
}

export default ReportSummaryScreen;

const styles = StyleSheet.create({
    //Container
    container: {
        flexDirection: 'row',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    leftSideContainer: {
        flex: 2,
    },
    innerLeftSideContainer: {
        flex: 1,
    },
    rightSideContainer: {
        flex: 1,
        paddingLeft: 5,
        paddingRight: 2,
    },
    innerRightSideContainer: {
        flex: 1,
    },

    //Text
    buttonTextStyle: {
        fontSize: 30,
        justifyContent: 'center',
    },

    formContainer: {
        alignItems: 'center',
        borderRadius: 20,
        height: '70%',
        width: '60%',
        backgroundColor: 'rgba(0,0,0,0.5)',
        justifyContent: 'center',
    },
    headerTextStyle: {
        fontSize: 40,
        alignSelf: 'center',
    },
    textStyle: {
        fontSize: 26,
    },
    inputStyle: {
        borderRadius: 15,
        backgroundColor: 'rgba(255,255,255,0.5)',
        marginTop: '2%',
        alignSelf: 'center',
        marginLeft: 30,
        paddingLeft: 10,
        marginRight: 30,
    },
    buttonStyle: {
        borderRadius: 0,
        marginLeft: 5,
        marginRight: 5,
    },
});
