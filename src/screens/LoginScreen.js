import React, { Component } from 'react';
import { View, Text, TextInput, KeyboardAvoidingView, ImageBackground } from 'react-native';
import { styles as gstyles } from '../styles/styles';
import { ButtonPrimary, ButtonSecondary } from './components/ButtonsComponent';
import { auth } from 'firebase/app';

import Spinner from 'react-native-loading-spinner-overlay';

class LoginScreen extends Component {
    state = {
        email: '',
        password: '',
        error_message: '',
        loading: false,
    };

    componentDidMount() {
        setTimeout(() => {
            this.setState({ loading: false });
            auth().onAuthStateChanged(user => this.props.navigation.navigate(user ? 'SelectScreen' : 'LoginScreen'));
        }, 3000);
        this.setState({ loading: true });
    }

    handleUserLogin = () => {
        let { email, password } = this.state;
        let { navigation } = this.props;

        this.setState({ loading: true });

        if (email && password) {
            auth()
                .signInWithEmailAndPassword(email, password)
                .then(() => {
                    this.setState({
                        loading: false,
                    });
                    navigation.navigate('SelectScreen');
                })
                .catch(error => {
                    this.setState({
                        error_message: 'ผู้ใช้งานหรือรหัสไม่ถูกต้อง',
                        loading: false,
                    });
                });
        } else {
            this.setState({
                error_message: 'โปรดกรอกผู้ใช้งาน',
                loading: false,
            });
        }
    };

    handleUserRegister = () => {
        let { navigation } = this.props;
        navigation.navigate('RegisterScreen');
    };

    render() {
        const { error_message, loading } = this.state;
        return (
            <ImageBackground source={require('../images/LoginBackground.jpg')} style={{ flex: 1 }} blurRadius={2}>
                <KeyboardAvoidingView style={[gstyles.container, { alignItems: 'center' }]} behavior="padding">
                    <Spinner visible={loading} />
                    <View style={[gstyles.formContainer, { width: '35%', alignItems: 'center' }]}>
                        <View style={gstyles.formElement}>
                            <Text style={gstyles.textTitle}>เข้าสู่ระบบ</Text>
                        </View>
                        <View style={gstyles.formRowElement}>
                            <View style={gstyles.inputContainer}>
                                <TextInput
                                    style={gstyles.inputPrimary}
                                    placeholder="ชื่อผู้ใช้งาน"
                                    placeholderTextColor="#404040"
                                    onChangeText={email => {
                                        this.setState({ email: email });
                                    }}
                                />
                            </View>
                        </View>
                        <View style={gstyles.formRowElement}>
                            <View style={gstyles.inputContainer}>
                                <TextInput
                                    style={gstyles.inputPrimary}
                                    placeholder="รหัสผ่าน"
                                    secureTextEntry
                                    autoCorrect={false}
                                    placeholderTextColor="#404040"
                                    onChangeText={password => {
                                        this.setState({ password: password });
                                    }}
                                />
                            </View>
                        </View>
                        <View style={gstyles.formRowElement}>
                            <Text style={gstyles.textH4}>{error_message}</Text>
                        </View>

                        <View style={gstyles.formRowElement}>
                            <ButtonPrimary name="เข้าสู่ระบบ" type="primary" onPress={() => this.handleUserLogin()} />
                            <ButtonSecondary name="ลงทะเบียน" type="secondary" onPress={() => this.handleUserRegister()} />
                        </View>
                    </View>
                </KeyboardAvoidingView>
            </ImageBackground>
        );
    }
}

export default LoginScreen;
