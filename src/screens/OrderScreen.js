import React, { Component } from 'react';
import { View, Text, StyleSheet, FlatList, Modal, TextInput } from 'react-native';

import { Button, Container, Footer, FooterTab, Content } from 'native-base';
import { FontAwesome } from '@expo/vector-icons';
import { styles as gstyles } from '../styles/styles';
import { ButtonPrimary, ButtonSecondary } from './components/ButtonsComponent';
import RNPickerSelect from 'react-native-picker-select';
import OrderHeader from './components/OrderHeader';

// SUB SCREEN
import VegetableOrderScreen from './subscreens/VegetableOrderScreen';
import SeafoodOrderScreen from './subscreens/SeafoodOrderScreen';
import PorkOrderScreen from './subscreens/PorkOrderScreen';
import ChickenOrderScreen from './subscreens/ChickenOrderScreen';
import BeefOrderScreen from './subscreens/BeefOrderScreen';
import OtherOrderScreen from './subscreens/OtherOrderScreen';

import ModalHeader from './components/ModalHeader';

import { connect } from 'react-redux';
import { removeItemFromCart, clearCart } from '../redux/actions/OrderAction';
import { toggleEdit } from '../redux/actions/StockAction';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { database } from 'firebase';

class OrderScreen extends Component {
    state = {
        order: [],
        index: 0,
        change: 0,
        check_bill: false,
        modal_visible: false,
        distance: { type: '', price: 0 },
    };

    handleSwitchScreen = index => {
        this.setState({ index: index });
    };

    handleRemoveItem = uid => {
        this.props.removeItemFromCart(uid);
    };

    handleCheckBill = () => {
        const { change, distance } = this.state;
        const { item, totalPrice } = this.props.order;
        if (distance.type === '') {
            return alert('โปรดระบุค่าจัดส่ง');
        }
        if (change < totalPrice + distance.price) {
            return alert('เงินไม่พอจ่าย');
        }

        let order = [];
        order.vegetable = [];
        order.seafood = [];
        order.pork = [];
        order.chicken = [];
        order.beef = [];
        order.other = [];
        for (i = 0; i < item.length; i++) {
            switch (item[i].type) {
                case 'ผัก':
                    database()
                        .ref('ingredians/vegetable/' + item[i].uid)
                        .update({ stock: item[i].stock - item[i].quantity });
                    delete item[i].stock;
                    order.vegetable.push(item[i]);
                    break;
                case 'ซีฟู๊ด':
                    database()
                        .ref('ingredians/seafood/' + item[i].uid)
                        .update({ stock: item[i].stock - item[i].quantity });
                    delete item[i].stock;
                    order.seafood.push(item[i]);
                    break;
                case 'หมู':
                    database()
                        .ref('ingredians/pork/' + item[i].uid)
                        .update({ stock: item[i].stock - item[i].quantity });
                    delete item[i].stock;
                    order.pork.push(item[i]);
                    break;
                case 'ไก่':
                    database()
                        .ref('ingredians/chicken/' + item[i].uid)
                        .update({ stock: item[i].stock - item[i].quantity });
                    delete item[i].stock;
                    order.chicken.push(item[i]);
                    break;
                case 'เนื้อ':
                    database()
                        .ref('ingredians/beef/' + item[i].uid)
                        .update({ stock: item[i].stock - item[i].quantity });
                    delete item[i].stock;
                    order.beef.push(item[i]);
                    break;
                case 'อื่นๆ':
                    database()
                        .ref('ingredians/other/' + item[i].uid)
                        .update({ stock: item[i].stock - item[i].quantity });
                    delete item[i].stock;
                    order.other.push(item[i]);
                    break;
                default:
                    break;
            }
        }

        let BillRef = database().ref('bills/' + this.Date());

        BillRef.push({
            bill: {
                item: order,
                distance: distance,
                total_price: totalPrice + distance.price,
            },
            date: this.DateTimeStamp(),
            time: this.TimeTimeStamp(),
        });
        this.setState({ check_bill: true });
    };

    Date = () => {
        var date = new Date().getDate();
        if (date < 10) date = '0' + date;
        var month = new Date().getMonth() + 1;
        if (month < 10) month = '0' + month;
        var year = new Date().getFullYear();
        let DateStamp = year + '/' + month + '/' + date;
        return DateStamp;
    };

    DateTimeStamp = () => {
        var date = new Date().getDate();
        if (date < 10) date = '0' + date;
        var month = new Date().getMonth() + 1;
        if (month < 10) month = '0' + month;
        var year = new Date().getFullYear();
        let DateTimeStamp = date + '/' + month + '/' + year;
        return DateTimeStamp;
    };

    TimeTimeStamp = () => {
        var hours = new Date().getHours();
        if (hours < 10) hours = '0' + hours;
        var min = new Date().getMinutes();
        if (min < 10) min = '0' + min;
        var sec = new Date().getSeconds();
        if (sec < 10) sec = '0' + sec;
        let TimeTimeStamp = hours + ':' + min + ':' + sec;
        return TimeTimeStamp;
    };

    toggleEditStock = () => {
        this.props.toggleEdit(this.props.stock.toggle_edit);
    };

    toggleModal(visible) {
        let { check_bill } = this.state;
        if (check_bill) {
            this.props.clearCart();
            this.setState({ check_bill: false, change: 0, modal_visible: visible });
            7;
        } else {
            this.setState({ modal_visible: visible });
        }
    }

    render() {
        let { toggle_edit } = this.props.stock;
        let { item, totalPrice } = this.props.order;
        const { navigation } = this.props;
        const { index, modal_visible, change, check_bill, distance } = this.state;
        const placeholder = {
            label: 'โปรดระบุระยะทาง',
            value: '',
            color: 'grey',
        };
        let OrderScreenIndex = null;
        switch (index) {
            case 0:
                OrderScreenIndex = VegetableOrderScreen;
                break;
            case 1:
                OrderScreenIndex = SeafoodOrderScreen;
                break;
            case 2:
                OrderScreenIndex = PorkOrderScreen;
                break;
            case 3:
                OrderScreenIndex = ChickenOrderScreen;
                break;
            case 4:
                OrderScreenIndex = BeefOrderScreen;
                break;
            case 5:
                OrderScreenIndex = OtherOrderScreen;
                break;
            default:
                break;
        }
        return (
            <View style={{ flex: 1, backgroundColor: 'lightgrey' }}>
                <OrderHeader navigation={navigation} title="รายการวัตถุดิบ" left_text="ย้อนกลับ" right_text={toggle_edit ? 'ยกเลิก' : 'อัพเดทสต็อกสินค้า'} onPress={() => this.toggleEditStock()} />
                <View style={styles.container}>
                    {/* SHOW ORDER */}
                    <View style={styles.leftSideContainer}>
                        <View style={styles.innerLeftSideContainer}>
                            <Content>
                                <OrderScreenIndex />
                            </Content>
                            <Footer>
                                <FooterTab style={{ backgroundColor: 'lightgrey' }}>
                                    <Button style={styles.buttonStyle} success active={index === 0} onPress={() => this.handleSwitchScreen(0)}>
                                        <Text style={styles.buttonTextStyle}>ผัก</Text>
                                    </Button>
                                    <Button style={styles.buttonStyle} success active={index === 1} onPress={() => this.handleSwitchScreen(1)}>
                                        <Text style={styles.buttonTextStyle}>ทะเล</Text>
                                    </Button>
                                    <Button style={styles.buttonStyle} success active={index === 2} onPress={() => this.handleSwitchScreen(2)}>
                                        <Text style={styles.buttonTextStyle}>หมู</Text>
                                    </Button>
                                    <Button style={styles.buttonStyle} success active={index === 3} onPress={() => this.handleSwitchScreen(3)}>
                                        <Text style={styles.buttonTextStyle}>ไก่</Text>
                                    </Button>
                                    <Button style={styles.buttonStyle} success active={index === 4} onPress={() => this.handleSwitchScreen(4)}>
                                        <Text style={styles.buttonTextStyle}>เนื้อ</Text>
                                    </Button>
                                    <Button style={styles.buttonStyle} success active={index === 5} onPress={() => this.handleSwitchScreen(5)}>
                                        <Text style={styles.buttonTextStyle}>อื่นๆ</Text>
                                    </Button>
                                </FooterTab>
                            </Footer>
                        </View>
                    </View>

                    {/* SHOW BILL */}
                    <View style={styles.rightSideContainer}>
                        <View style={styles.innerRightSideContainer}>
                            <Container
                                style={{
                                    height: '80%',
                                    width: '100%',
                                    marginTop: 20,
                                    marginBottom: 20,
                                    borderWidth: 2,
                                }}
                            >
                                <View
                                    style={{
                                        flexDirection: 'row',
                                        height: '10%',
                                        width: '95%',
                                        borderBottomWidth: 2,
                                        borderBottomColor: 'black',
                                        alignItems: 'center',
                                        alignSelf: 'center',
                                    }}
                                >
                                    <View style={{ width: '40%' }}>
                                        <Text style={styles.textStyle}>รายการ</Text>
                                    </View>
                                    <View style={{ width: '25%', alignItems: 'center' }}>
                                        <Text style={styles.textStyle}>จำนวน</Text>
                                    </View>
                                    <View style={{ width: '35%', alignItems: 'flex-end' }}>
                                        <Text style={styles.textStyle}>ราคา</Text>
                                    </View>
                                </View>
                                <FlatList
                                    data={item}
                                    keyExtractor={item => item.uid}
                                    renderItem={({ item, index }) => (
                                        <View
                                            style={{
                                                flexDirection: 'row',
                                                height: 40,
                                                width: '95%',
                                                alignItems: 'center',
                                                alignSelf: 'center',
                                            }}
                                        >
                                            <View style={{ width: '40%', flexDirection: 'row', alignItems: 'center' }}>
                                                <TouchableOpacity style={{ width: 20, height: 20 }} onPress={() => this.handleRemoveItem(item.uid)}>
                                                    <FontAwesome name="minus" size={20} />
                                                </TouchableOpacity>
                                                <Text style={styles.textStyle}>{item.name}</Text>
                                            </View>
                                            <View style={{ width: '25%', alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }}>
                                                <Text style={styles.textStyle}>{item.quantity}</Text>
                                            </View>
                                            <View style={{ width: '35%', alignItems: 'flex-end' }}>
                                                <Text style={styles.textStyle}>
                                                    {parseFloat(item.sum_price)
                                                        .toFixed(2)
                                                        .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}
                                                </Text>
                                            </View>
                                        </View>
                                    )}
                                />
                                <Footer>
                                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                                        <Text>ยอดรวมทั้งสิ้น </Text>

                                        <Text>
                                            {parseFloat(totalPrice)
                                                .toFixed(2)
                                                .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}
                                        </Text>
                                        <Text> บาท</Text>
                                    </View>
                                </Footer>
                            </Container>
                        </View>
                        <Footer style={{ marginBottom: 40 }}>
                            <FooterTab>
                                <Button
                                    onPress={() => {
                                        if (totalPrice > 0) {
                                            this.toggleModal(!modal_visible);
                                        }
                                    }}
                                    warning
                                    style={{
                                        flex: 1,
                                        alignSelf: 'center',
                                        borderRadius: 0,
                                        borderWidth: 2,
                                        borderColor: 'black',
                                        justifyContent: 'center',
                                        width: '100%',
                                    }}
                                >
                                    <Text style={styles.buttonTextStyle}>ชำระเงิน</Text>
                                </Button>
                            </FooterTab>
                        </Footer>
                    </View>
                </View>
                <Modal animationType="fade" transparent={false} presentationStyle="formSheet" visible={modal_visible}>
                    <View style={{ flex: 1 }}>
                        <ModalHeader title="ชำระเงิน" rightEvent={() => this.toggleModal(!modal_visible)} />
                        <View style={gstyles.container}>
                            <View style={{ flex: 1 }}>
                                <View style={gstyles.formRowElement}>
                                    <View style={{ flex: 3 }}>
                                        <View
                                            style={{
                                                margin: 5,
                                                alignSelf: 'stretch',
                                                alignItems: 'center',
                                                flexDirection: 'row',
                                                justifyContent: 'space-between',
                                                paddingLeft: 15,
                                                paddingRight: 15,
                                                height: 45,
                                            }}
                                        >
                                            <Text style={gstyles.textH2}>ระยะทาง</Text>
                                            <Text style={gstyles.textH2}>:</Text>
                                        </View>
                                    </View>
                                    <View style={{ flex: 4 }}>
                                        <View style={gstyles.inputContainer}>
                                            <View style={[gstyles.inputPrimary, { justifyContent: 'center' }]}>
                                                <RNPickerSelect
                                                    placeholder={placeholder}
                                                    value={distance.type}
                                                    placeholderTextColor={distance.type !== '' ? 'grey' : 'black'}
                                                    color="black"
                                                    onValueChange={value => {
                                                        if (value === '') {
                                                            this.setState({ distance: { type: '', price: 0 } });
                                                        }
                                                        if (value === 'ใกล้') {
                                                            this.setState({ distance: { type: value, price: 0 } });
                                                        }
                                                        if (value === 'กลาง') {
                                                            this.setState({ distance: { type: value, price: 5 } });
                                                        }
                                                        if (value === 'ไกล') {
                                                            this.setState({ distance: { type: value, price: 10 } });
                                                        }
                                                    }}
                                                    items={[
                                                        { label: 'ใกล้', value: 'ใกล้', color: 'black' },
                                                        { label: 'กลาง', value: 'กลาง', color: 'black' },
                                                        { label: 'ไกล', value: 'ไกล', color: 'black' },
                                                    ]}
                                                />
                                            </View>
                                        </View>
                                    </View>
                                    <View style={{ flex: 5 }}>
                                        <View
                                            style={{
                                                margin: 5,
                                                alignSelf: 'stretch',
                                                alignItems: 'center',
                                                flexDirection: 'row',
                                                justifyContent: 'flex-end',
                                                paddingLeft: 15,
                                                paddingRight: 15,
                                                height: 45,
                                            }}
                                        >
                                            <Text style={gstyles.textTitle}>
                                                {parseFloat(distance.price)
                                                    .toFixed(2)
                                                    .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}{' '}
                                                บาท
                                            </Text>
                                        </View>
                                    </View>
                                </View>

                                <View style={gstyles.formRowElement}>
                                    <View style={{ flex: 3 }}>
                                        <View
                                            style={{
                                                margin: 5,
                                                alignSelf: 'stretch',
                                                alignItems: 'center',
                                                flexDirection: 'row',
                                                justifyContent: 'space-between',
                                                paddingLeft: 15,
                                                paddingRight: 15,
                                                height: 45,
                                            }}
                                        >
                                            <Text style={gstyles.textH2}>ราคารวม</Text>
                                            <Text style={gstyles.textH2}>:</Text>
                                        </View>
                                    </View>
                                    <View style={{ flex: 9 }}>
                                        <View
                                            style={{
                                                margin: 5,
                                                alignSelf: 'stretch',
                                                alignItems: 'center',
                                                flexDirection: 'row',
                                                justifyContent: 'flex-end',
                                                paddingLeft: 15,
                                                paddingRight: 15,
                                                height: 45,
                                            }}
                                        >
                                            <Text style={gstyles.textTitle}>
                                                {parseFloat(totalPrice + distance.price)
                                                    .toFixed(2)
                                                    .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}{' '}
                                                บาท
                                            </Text>
                                        </View>
                                    </View>
                                </View>
                                <View style={gstyles.formRowElement}>
                                    <View style={{ flex: 3 }}>
                                        <View
                                            style={{
                                                margin: 5,
                                                alignSelf: 'stretch',
                                                alignItems: 'center',
                                                flexDirection: 'row',
                                                justifyContent: 'space-between',
                                                paddingLeft: 15,
                                                paddingRight: 15,
                                                height: 45,
                                            }}
                                        >
                                            <Text style={gstyles.textH2}>จำนวนเงิน</Text>
                                            <Text style={gstyles.textH2}>:</Text>
                                        </View>
                                    </View>
                                    <View style={{ flex: 9 }}>
                                        <View style={gstyles.inputContainer}>
                                            <TextInput
                                                keyboardType="numeric"
                                                style={gstyles.inputPrimary}
                                                placeholder="โปรดระบุจำนวนเงิน"
                                                placeholderTextColor="grey"
                                                onChangeText={change => {
                                                    this.setState({ change: parseFloat(change) });
                                                }}
                                            />
                                        </View>
                                    </View>
                                </View>
                                <View style={gstyles.row}>
                                    <ButtonPrimary name="ยกเลิก" type="primary" onPress={() => this.toggleModal(!modal_visible)} />
                                    <ButtonSecondary name="ชำระสินค้า" type="secondary" onPress={() => this.handleCheckBill()} />
                                </View>
                                <View style={{ width: '90%', borderTopWidth: 2, borderTopColor: 'black', alignSelf: 'center', marginTop: '2%' }} />
                                {check_bill ? (
                                    <View>
                                        <View style={gstyles.formRowElement}>
                                            <View style={{ flex: 3 }}>
                                                <View
                                                    style={{
                                                        margin: 5,
                                                        alignSelf: 'stretch',
                                                        alignItems: 'center',
                                                        flexDirection: 'row',
                                                        justifyContent: 'space-between',
                                                        paddingLeft: 15,
                                                        paddingRight: 15,
                                                        height: 45,
                                                    }}
                                                >
                                                    <Text style={gstyles.textH2}>ราคารวม</Text>
                                                    <Text style={gstyles.textH2}>:</Text>
                                                </View>
                                            </View>
                                            <View style={{ flex: 9 }}>
                                                <View
                                                    style={{
                                                        margin: 5,
                                                        alignSelf: 'stretch',
                                                        alignItems: 'center',
                                                        flexDirection: 'row',
                                                        justifyContent: 'flex-end',
                                                        paddingLeft: 15,
                                                        paddingRight: 15,
                                                        height: 45,
                                                    }}
                                                >
                                                    <Text style={gstyles.textTitle}>
                                                        {parseFloat(totalPrice + distance.price)
                                                            .toFixed(2)
                                                            .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}{' '}
                                                        บาท
                                                    </Text>
                                                </View>
                                            </View>
                                        </View>
                                        <View style={gstyles.formRowElement}>
                                            <View style={{ flex: 3 }}>
                                                <View
                                                    style={{
                                                        margin: 5,
                                                        alignSelf: 'stretch',
                                                        alignItems: 'center',
                                                        flexDirection: 'row',
                                                        justifyContent: 'space-between',
                                                        paddingLeft: 15,
                                                        paddingRight: 15,
                                                        height: 45,
                                                    }}
                                                >
                                                    <Text style={gstyles.textH2}>จำนวนเงิน</Text>
                                                    <Text style={gstyles.textH2}>:</Text>
                                                </View>
                                            </View>
                                            <View style={{ flex: 9 }}>
                                                <View
                                                    style={{
                                                        margin: 5,
                                                        alignSelf: 'stretch',
                                                        alignItems: 'center',
                                                        flexDirection: 'row',
                                                        justifyContent: 'flex-end',
                                                        paddingLeft: 15,
                                                        paddingRight: 15,
                                                        height: 45,
                                                    }}
                                                >
                                                    <Text style={gstyles.textTitle}>
                                                        {parseFloat(change)
                                                            .toFixed(2)
                                                            .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}{' '}
                                                        บาท
                                                    </Text>
                                                </View>
                                            </View>
                                        </View>
                                        <View style={gstyles.formRowElement}>
                                            <View style={{ flex: 3 }}>
                                                <View
                                                    style={{
                                                        margin: 5,
                                                        alignSelf: 'stretch',
                                                        alignItems: 'center',
                                                        flexDirection: 'row',
                                                        justifyContent: 'space-between',
                                                        paddingLeft: 15,
                                                        paddingRight: 15,
                                                        height: 45,
                                                    }}
                                                >
                                                    <Text style={gstyles.textH2}>เงินทอน</Text>
                                                    <Text style={gstyles.textH2}>:</Text>
                                                </View>
                                            </View>
                                            <View style={{ flex: 9 }}>
                                                <View
                                                    style={{
                                                        margin: 5,
                                                        alignSelf: 'stretch',
                                                        alignItems: 'center',
                                                        flexDirection: 'row',
                                                        justifyContent: 'flex-end',
                                                        paddingLeft: 15,
                                                        paddingRight: 15,
                                                        height: 45,
                                                    }}
                                                >
                                                    <Text style={gstyles.textTitle}>
                                                        {parseFloat(change - (totalPrice + distance.price))
                                                            .toFixed(2)
                                                            .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}{' '}
                                                        บาท
                                                    </Text>
                                                </View>
                                            </View>
                                        </View>
                                        <View style={gstyles.row}>
                                            <ButtonPrimary
                                                name="เสร็จสิ้น"
                                                type="primary"
                                                onPress={() => {
                                                    this.props.clearCart();
                                                    this.setState({ check_bill: false, change: 0 });
                                                    this.toggleModal(!modal_visible);
                                                }}
                                            />
                                        </View>
                                        <View style={{ width: '90%', borderTopWidth: 2, borderTopColor: 'black', alignSelf: 'center', marginTop: '2%' }} />
                                    </View>
                                ) : (
                                    <View></View>
                                )}
                            </View>
                        </View>
                    </View>
                </Modal>
            </View>
        );
    }
}

const mapStateToProps = state => {
    return {
        order: state.order,
        stock: state.stock,
    };
};

export default connect(mapStateToProps, { removeItemFromCart, clearCart, toggleEdit })(OrderScreen);

const styles = StyleSheet.create({
    //Container
    container: {
        flexDirection: 'row',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    leftSideContainer: {
        flex: 2,
    },
    innerLeftSideContainer: {
        flex: 1,
    },
    rightSideContainer: {
        flex: 1,
        paddingLeft: 5,
        paddingRight: 2,
    },
    innerRightSideContainer: {
        flex: 1,
    },

    //Text
    buttonTextStyle: {
        fontSize: 30,
        justifyContent: 'center',
    },

    formContainer: {
        alignItems: 'center',
        borderRadius: 20,
        height: '70%',
        width: '60%',
        backgroundColor: 'rgba(0,0,0,0.5)',
        justifyContent: 'center',
    },
    headerTextStyle: {
        fontSize: 40,
        alignSelf: 'center',
    },
    textStyle: {
        fontSize: 26,
    },
    inputStyle: {
        borderRadius: 15,
        backgroundColor: 'rgba(255,255,255,0.5)',
        marginTop: '2%',
        alignSelf: 'center',
        marginLeft: 30,
        paddingLeft: 10,
        marginRight: 30,
    },
    buttonStyle: {
        borderRadius: 0,
        marginLeft: 5,
        marginRight: 5,
    },
});
