import React, { Component } from 'react';
import { View, Text, TextInput, KeyboardAvoidingView, ImageBackground } from 'react-native';
import { styles as gstyles } from '../styles/styles';
import { ButtonPrimary, ButtonDanger } from './components/ButtonsComponent';
import { auth, database } from 'firebase/app';

import Spinner from 'react-native-loading-spinner-overlay';

class RegisterScreen extends Component {
    state = {
        name: '',
        email: '',
        phone: '',
        password: '',
        confirm_password: '',
        error_message: '',
        loading: false,
    };

    userRegister = () => {
        const { name, email, phone, password, confirm_password } = this.state;
        const { navigation } = this.props;
        this.setState({ loading: true });

        if (password === confirm_password) {
            auth()
                .createUserWithEmailAndPassword(email, password)
                .then(() => {
                    auth()
                        .signInWithEmailAndPassword(email, password)
                        .then(() => {
                            const role = 'seller';
                            const user = auth().currentUser;
                            const auth_uid = user.uid;
                            user.updateProfile({
                                displayName: name,
                            }).then(() => {
                                const seller = database().ref('users/sellers/' + auth_uid);
                                seller
                                    .set({
                                        name: name,
                                        role: role,
                                        email: email,
                                        phone: phone,
                                    })
                                    .then(() => {
                                        auth().signOut();
                                        this.setState({ loading: false });
                                        navigation.navigate('LoginScreen');
                                    });
                            });
                        });
                })
                .catch(err => {
                    this.setState({ error_message: 'ไม่สามารถทำรายการได้', loading: false });
                });
        } else {
            this.setState({ error_message: 'รหัสผ่านไม่ตรงกัน', loading: false });
        }
    };

    userCancelRegister = () => {
        const { navigation } = this.props;
        navigation.navigate('LoginScreen');
    };

    render() {
        const { error_message, loading } = this.state;

        return (
            <ImageBackground source={require('../images/LoginBackground.jpg')} style={{ flex: 1 }} blurRadius={2}>
                <KeyboardAvoidingView style={[gstyles.container, { alignItems: 'center' }]} behavior="padding">
                    <Spinner visible={loading} />
                    <View style={[gstyles.formContainer, { width: '60%' }]}>
                        <View style={gstyles.formElement}>
                            <Text style={gstyles.textTitle}>ลงทะเบียน</Text>
                        </View>

                        <View style={gstyles.formRowElement}>
                            <View style={gstyles.inputContainer}>
                                <TextInput style={gstyles.inputPrimary} placeholder="ชื่อ - นามสกุล" placeholderTextColor="#404040" onChangeText={name => this.setState({ name: name })} />
                            </View>
                        </View>
                        <View style={gstyles.formRowElement}>
                            <View style={gstyles.inputContainer}>
                                <TextInput style={gstyles.inputPrimary} placeholder="อีเมล์" placeholderTextColor="#404040" onChangeText={email => this.setState({ email: email })} />
                            </View>
                        </View>
                        <View style={gstyles.formRowElement}>
                            <View style={gstyles.inputContainer}>
                                <TextInput
                                    style={gstyles.inputPrimary}
                                    secureTextEntry
                                    autoCorrect={false}
                                    placeholder="รหัสผ่าน"
                                    placeholderTextColor="#404040"
                                    onChangeText={password => this.setState({ password: password })}
                                />
                            </View>
                        </View>
                        <View style={gstyles.formRowElement}>
                            <View style={gstyles.inputContainer}>
                                <TextInput
                                    style={gstyles.inputPrimary}
                                    secureTextEntry
                                    autoCorrect={false}
                                    placeholder="ยืนยันรหัสผ่าน"
                                    placeholderTextColor="#404040"
                                    onChangeText={confirm_password => this.setState({ confirm_password: confirm_password })}
                                />
                            </View>
                        </View>
                        <View style={gstyles.formRowElement}>
                            <Text style={gstyles.textH4}>{error_message}</Text>
                        </View>
                        <View style={gstyles.formRowElement}>
                            <ButtonDanger
                                name="ยกเลิก"
                                type="primary"
                                onPress={() => {
                                    this.userCancelRegister();
                                }}
                            />
                            <ButtonPrimary
                                name="ลงทะเบียน"
                                type="primary"
                                onPress={() => {
                                    this.userRegister();
                                }}
                            />
                        </View>
                    </View>
                </KeyboardAvoidingView>
            </ImageBackground>
        );
    }
}

export default RegisterScreen;
