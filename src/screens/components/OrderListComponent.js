import React from 'react';
import { StyleSheet, View, FlatList } from 'react-native';

function OrderList(props) {
  return (
    <View>
      <FlatList
        data={props}
        keyExtractor={props => props._id}
        renderItem={({ item, index }) => {
          <View>
            <Text>{item.NAME}</Text>
            <Text>{item.TYPE}</Text>
            <Text>{item.PRICE}</Text>
            <Text>{item.PACKAGE}</Text>
          </View>;
        }}
      />
    </View>
  );
}

export default OrderList;
