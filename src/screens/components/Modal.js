import React, { Component } from 'react';
import { Text, View, TouchableOpacity, Image, TextInput } from 'react-native';
import { Header, Left, Right, Icon, Body, Title } from 'native-base';
import { styles as gstyles, ButtonPrimary, ButtonSecondary } from '../../styles/styles';
import { FontAwesome } from '@expo/vector-icons';

class Modal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ingredians: [],
            name: '',
            type: '',
            price: '',
            package_type: '',
            modal_visible: false,
            edit_name: '',
            image: '',
            loading: false,
        };
    }

    render(props) {
        // VARIABLE
        const { visible, toggleModal } = this.props;
        const { image, name, type, price } = this.state;
        // FUNCTION
        return (
            <Modal animationType="fade" transparent={false} presentationStyle="formSheet" visible={visible}>
                <View style={{ flex: 1 }}>
                    <Header style={{ backgroundColor: '#9BBA74' }} androidStatusBarColor="#9BBA74">
                        <Left />
                        <Body>
                            <Title>เพิ่มรายการวัตถุดิบ</Title>
                        </Body>
                        <Right>
                            <Icon name="close" onPress={toggleModal} />
                        </Right>
                    </Header>
                    <View style={gstyles.container}>
                        <TouchableOpacity
                            // onPress={() => this.uploadImage()}
                            style={{
                                borderWidth: 2,
                                overflow: 'hidden',
                                borderRadius: 50,
                                borderColor: 'black',
                                width: 120,
                                height: 120,
                                borderRadius: 5,
                            }}
                        >
                            <View style={{ width: 120, height: 120, borderRadius: 5 }}>
                                {image ? (
                                    <Image
                                        overflow="hidden"
                                        style={{ width: 120, height: 120, overflow: 'hidden' }}
                                        source={{
                                            uri: image ? image : '',
                                        }}
                                    />
                                ) : (
                                    <FontAwesome name="image" size={30} />
                                )}
                            </View>
                        </TouchableOpacity>
                        <View style={gstyles.formRowElement}>
                            <View style={gstyles.inputContainer}>
                                <TextInput
                                    style={gstyles.inputPrimary}
                                    placeholder="ชื่อสินค้า"
                                    placeholderTextColor="#404040"
                                    onChangeText={name => {
                                        this.setState({ name: name });
                                    }}
                                />
                            </View>
                        </View>
                        <View style={gstyles.formRowElement}>
                            <View style={gstyles.inputContainer}>
                                <TextInput
                                    style={gstyles.inputPrimary}
                                    placeholder="ราคาขาย"
                                    placeholderTextColor="#404040"
                                    onChangeText={price => {
                                        this.setState({ price: price });
                                    }}
                                />
                            </View>
                        </View>
                        <View style={gstyles.formRowElement}>
                            <View style={gstyles.inputContainer}>
                                <TextInput
                                    style={gstyles.inputPrimary}
                                    placeholder="หมวด"
                                    placeholderTextColor="#404040"
                                    onChangeText={type => {
                                        this.setState({ type: type });
                                    }}
                                />
                            </View>
                        </View>
                        <View style={gstyles.formRowElement}>
                            <View style={gstyles.inputContainer}>
                                <TextInput
                                    style={gstyles.inputPrimary}
                                    placeholder="วิธีการขาย"
                                    placeholderTextColor="#404040"
                                    onChangeText={package_type => {
                                        this.setState({ package_type: package_type });
                                    }}
                                />
                            </View>
                        </View>
                        <View style={gstyles.row}>
                            <ButtonPrimary name="ยกเลิก" type="primary" />
                            <ButtonSecondary name="เพิ่มรายการวัตถุดิบ" type="secondary" />
                        </View>
                    </View>
                </View>
            </Modal>
        );
    }
}

export default Modal;
