import React, { Component } from 'react';
import { TouchableOpacity } from 'react-native';
import { NavigationActions, StackActions } from 'react-navigation';
import { Header, Left, Right, Icon, Body, Title } from 'native-base';
import { primaryColor } from '../../styles/styles';

import { auth } from 'firebase/app';

class SelectHeader extends Component {
    state = { displayName: '' };
    componentDidMount() {
        auth().onAuthStateChanged(user => this.setState({ displayName: user ? auth().currentUser.displayName : '' }));
    }

    handleUserSignOut = () => {
        alert('sign out');
        auth().signOut();
        this.props.navigation.dispatch(NavigationActions.back());
    };

    render() {
        let { displayName } = this.state;
        let { navigation, title, left_text, right_text } = this.props;
        return (
            <Header style={{ backgroundColor: primaryColor.green }} androidStatusBarColor={primaryColor.green}>
                <Left>{/* <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', paddingLeft: '1%' }} onPress={() => navigation.dispatch(NavigationActions.back())}>
                        <Icon type="FontAwesome" name="arrow-left" style={{ color: 'lightgrey' }} />
                        <Title style={{ paddingLeft: 10, color: 'lightgrey' }}>{left_text}</Title>
                    </TouchableOpacity> */}</Left>
                <Body style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                    <Title style={{ paddingRight: 10, color: 'lightgrey' }}>{title}</Title>
                </Body>
                <Right>
                    <TouchableOpacity onPress={() => this.handleUserSignOut()} style={{ flexDirection: 'row', alignItems: 'center', paddingRight: '1%' }}>
                        <Title style={{ paddingRight: 10, color: 'lightgrey' }}>ยินดีต้อนรับ คุณ {displayName}</Title>
                        <Icon type="FontAwesome" name="sign-out" style={{ color: 'lightgrey' }} />
                    </TouchableOpacity>
                </Right>
            </Header>
        );
    }
}

export default SelectHeader;
