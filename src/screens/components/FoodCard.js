import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Dialog from 'react-native-dialog';
class FoodCard extends Component {
    state = {};

    showDialog = () => {
        this.setState({ dialogVisible: true });
    };

    handleCancel = () => {
        this.setState({ dialogVisible: false });
    };

    handleDelete = () => {
        // The user has pressed the "Delete" button, so here you can do your own logic.
        // ...Your logic
        this.setState({ dialogVisible: false });
    };

    render() {
        const { name, type, price, package_type, image_uri } = this.props.item;
        return (
            <View style={styles.container}>
                <TouchableOpacity style={styles.innerContainer} onPress={this.showDialog}>
                    <Image source={{ uri: image_uri }} style={styles.imageStyle}></Image>
                    <Text style={styles.textStyle}>{name}</Text>
                    <Text style={styles.textStyle}>
                        {price}/{package_type}
                    </Text>
                </TouchableOpacity>

                <Dialog.Container visible={this.state.dialogVisible}>
                    <Dialog.Title>น้ำหนัก</Dialog.Title>
                    <Dialog.Input></Dialog.Input>
                    <Dialog.Description>โปรดระบุน้ำหนักที่ต้องการ</Dialog.Description>
                    <Dialog.Button label="ยกเลิก" onPress={this.handleCancel} />
                    <Dialog.Button label="ยืนยัน" onPress={this.handleDelete} />
                </Dialog.Container>
            </View>
        );
    }
}

export default FoodCard;

const styles = StyleSheet.create({
    //Container
    container: {
        flex: 1,

        alignItems: 'center',
        justifyContent: 'center',
    },
    innerContainer: {
        flex: 1,
        margin: 10,
        width: wp('15%'),
        height: hp('30%'),
        borderColor: 'black',
        alignItems: 'center',
        justifyContent: 'center',

        borderWidth: 1,
    },
    imageStyle: {
        width: wp('10%'),
        height: hp('20%'),
    },

    leftSideContainer: {
        flex: 2,
        backgroundColor: 'green',
    },
    innerLeftSideContainer: {
        flex: 1,
    },
    rightSideContainer: {
        flex: 1,
        backgroundColor: 'red',
    },
    innerRightSideContainer: {
        flex: 1,
    },

    //Text
    buttonTextStyle: {
        fontSize: 30,
        alignSelf: 'center',
    },

    formContainer: {
        alignItems: 'center',
        borderRadius: 20,
        height: hp('70%'),
        width: wp('60%'),
        backgroundColor: 'rgba(0,0,0,0.5)',
        justifyContent: 'center',
    },
    headerTextStyle: {
        fontSize: hp('10%'),
        padding: 10,
        color: 'white',
        alignSelf: 'center',
    },
    textStyle: {
        fontSize: 26,
        alignSelf: 'center',
    },
    inputStyle: {
        borderRadius: 15,
        backgroundColor: 'rgba(255,255,255,0.5)',
        marginTop: hp('2%'),
        alignSelf: 'center',
        marginLeft: 30,
        paddingLeft: 10,
        marginRight: 30,
    },
    buttonStyle: {
        alignSelf: 'center',
        marginLeft: 45,
        marginRight: 45,
        justifyContent: 'center',
        marginTop: 10,
        width: wp('20%'),
        borderRadius: 15,
    },
});
