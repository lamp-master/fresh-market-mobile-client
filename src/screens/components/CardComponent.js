import React from "react";
import { Text, View, FlatList } from "react-native";
import { styles as gstyles } from "../../styles/styles";

export const IngredianCard = function(props) {
  let { ingredians } = props;
  return (
    <View>
      <FlatList data={ingredians} keyExtractor={ingredians => ingredians._id} />
    </View>
  );
};
