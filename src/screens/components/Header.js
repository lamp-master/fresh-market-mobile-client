import React from 'react';
import { NavigationActions, StackActions } from 'react-navigation';
import { Header, Left, Right, Icon, Body, Title } from 'native-base';
import { primaryColor } from '../../styles/styles';

import { auth } from 'firebase/app';

const HeaderComponent = props => {
    let { navigation, title, left_text, right_text } = props;
    return (
        <Header style={{ backgroundColor: primaryColor.green }} androidStatusBarColor={primaryColor.green}>
            <Left>
                <Icon name="menu" style={{ color: 'white' }} onPress={() => navigation.dispatch(NavigationActions.back())} />
                <Title style={{ paddingLeft: 10 }}>{left_text}</Title>
            </Left>
            <Body>
                <Title style={{ paddingRight: 10 }}>{title}</Title>
            </Body>
            <Right style={{ alignItems: 'center' }}>
                <Title style={{ paddingRight: 10 }}>{right_text}</Title>
                <Icon
                    name="log-out"
                    style={{ color: 'white' }}
                    onPress={() => {
                        auth()
                            .signOut()
                            .then(() => {
                                navigation.dispatch(NavigationActions.back());
                            })
                            .catch(err => {
                                alert(err);
                            });
                    }}
                />
            </Right>
        </Header>
    );
};

export default HeaderComponent;
