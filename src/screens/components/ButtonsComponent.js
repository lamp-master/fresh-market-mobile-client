import React from "react";
import { Text, TouchableOpacity, StyleSheet } from "react-native";
import {
  styles as gstyles,
  primaryColor,
  secondaryColor
} from "../../styles/styles";

export const ButtonPrimary = function(props) {
  const { name, type, onPress } = props;
  return (
    <TouchableOpacity
      style={[
        styles.button,
        {
          borderColor:
            type === "primary" ? primaryColor.green : secondaryColor.green
        }
      ]}
      onPress={onPress}
    >
      <Text
        style={[
          gstyles.textH3,
          {
            color:
              type === "primary" ? primaryColor.green : secondaryColor.green
          }
        ]}
      >
        {name}
      </Text>
    </TouchableOpacity>
  );
};

export const ButtonSecondary = function(props) {
  const { name, type, onPress } = props;
  return (
    <TouchableOpacity
      style={[
        styles.button,
        {
          borderColor:
            type === "primary" ? primaryColor.blue : secondaryColor.blue
        }
      ]}
      onPress={onPress}
    >
      <Text
        style={[
          gstyles.textH3,
          {
            color: type === "primary" ? primaryColor.blue : secondaryColor.blue
          }
        ]}
      >
        {name}
      </Text>
    </TouchableOpacity>
  );
};

export const ButtonDanger = function(props) {
  const { name, type, onPress } = props;
  return (
    <TouchableOpacity
      style={[
        styles.button,
        {
          borderColor:
            type === "primary" ? primaryColor.brown : secondaryColor.brown
        }
      ]}
      onPress={onPress}
    >
      <Text
        style={[
          gstyles.textH3,
          {
            color:
              type === "primary" ? primaryColor.brown : secondaryColor.brown
          }
        ]}
      >
        {name}
      </Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    margin: 5,
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 10,
    paddingRight: 10,

    alignItems: "center",
    alignSelf: "center",

    borderWidth: 2,
    borderRadius: 15
  }
});
