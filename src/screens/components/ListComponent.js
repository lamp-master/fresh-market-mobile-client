import React from "react";
import { Text, View, FlatList } from "react-native";
import { styles as gstyles } from "../../styles/styles";

export const IngredianList = function(props) {
  let { ingredians } = props;
  return (
    <View>
      <FlatList
        data={ingredians}
        keyExtractor={ingredians => ingredians._id}
        renderItem={({ item, index }) => (
          <View style={{}}>
            {/* INGREDIAN DETAIL */}
            <View style={{ flex: 1 }}>
              <View style={[gstyles.row, { backgroundColor: "lightgrey" }]}>
                <Text style={gstyles.textTitle}>
                  {" "}
                  ชื่อ : {ingredians.name}{" "}
                </Text>
                <FontAwesome name="edit" size={30} color="grey" />
              </View>

              <Text style={gstyles.textSubtitle}>
                {" "}
                ชนิด : {ingredians.type}{" "}
              </Text>
              <Text style={gstyles.textH2}> ราคา : {ingredians.price} </Text>
              <Text style={gstyles.textH2}>
                จำนวนที่เหลือในสต็อก : {ingredians.quantity}
              </Text>
            </View>

            {/* INGREDIAN PROCESS */}
            <View style={{}}>
              <ButtonPrimary
                name="อัพเดทสต็อก"
                type="primary"
                onPress={() => this.updateQuantity(id, quantity)}
              />
              <ButtonSecondary
                name="อัพเดทราคาปัจจุบัน"
                type="primary"
                onPress={() => this.updatePrice(id, price)}
              />
              <ButtonDanger
                name="ลบรายการนี้ถาวร"
                type="primary"
                onPress={() => this.deleteIngredian(id)}
              />
            </View>
          </View>
        )}
      />
    </View>
  );
};
