import React from 'react';
import { NavigationActions } from 'react-navigation';
import { Header, Left, Right, Icon, Body, Title } from 'native-base';

const HeaderComponent = props => {
    let { title, rightEvent } = props;
    return (
        <Header style={{ backgroundColor: '#9BBA74' }} androidStatusBarColor="#9BBA74">
            <Left />
            <Body>
                <Title>{title}</Title>
            </Body>
            <Right>
                <Icon name="close" onPress={rightEvent} />
            </Right>
        </Header>
    );
};

export default HeaderComponent;
