import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, StyleSheet } from 'react-native';
import { styles as gstyles, secondaryColor } from '../styles/styles';
import { auth } from 'firebase/app';

import SelectHeader from './components/SelectHeader';

class SelectScreen extends Component {
    render() {
        const { navigation } = this.props;
        return (
            <View style={{ flex: 1 }}>
                <SelectHeader navigation={navigation} />
                <View style={gstyles.rowContainer}>
                    <TouchableOpacity style={[gstyles.container, { alignItems: 'center' }]} onPress={() => navigation.navigate('OrderScreen')}>
                        <Image source={require('../images/OrderMenu.png')} style={styles.image} />
                        <Text style={gstyles.textH1}>ซื้อ - ขาย</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={[gstyles.container, { alignItems: 'center', backgroundColor: secondaryColor.green }]} onPress={() => navigation.navigate('ReportSummaryScreen')}>
                        <Image source={require('../images/ManageMenu.png')} style={styles.image} />
                        <Text style={gstyles.textH1}>สรุปยอดซื้อ - ขาย</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    image: {
        width: '80%',
        height: '25%',
        resizeMode: 'contain',
    },
});

export default SelectScreen;
