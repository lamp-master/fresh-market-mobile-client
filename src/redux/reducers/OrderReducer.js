import { ADD_ITEM_TO_CART, ADD_QUANTITY, SUB_QUANTITY, REMOVE_ITEM_FROM_CART, CLEAR_CART, UPDATE_STOCK } from '../actions/ActionType.js';

const initState = {
    item: [],
    distance: 0,
    totalPrice: 0,
};

const OrderReducer = (state = initState, action) => {
    let existed_item = null;
    let new_quantity = null;

    switch (action.type) {
        case ADD_ITEM_TO_CART:
            let item = action.payload;
            existed_item = state.item.find(item => item.uid === action.payload.uid);
            if (existed_item) {
                if (existed_item.quantity + action.payload.quantity > existed_item.stock) {
                    setTimeout(function() {
                        alert('สินค้าที่มีอยู่ไม่พอ');
                    }, 1000);
                    return state;
                } else {
                    new_quantity = state.item.map(item =>
                        item.uid === action.payload.uid
                            ? {
                                  ...item,
                                  quantity: parseFloat(existed_item.quantity.toFixed(2)) + parseFloat(action.payload.quantity.toFixed(2)),
                                  sum_price: parseFloat(existed_item.sum_price.toFixed(2)) + parseFloat(item.price.toFixed(2)) * parseFloat(action.payload.quantity.toFixed(2)),
                              }
                            : item,
                    );
                    return {
                        ...state,
                        item: new_quantity,
                        totalPrice: parseFloat(state.totalPrice.toFixed(2)) + parseFloat(existed_item.price.toFixed(2)) * parseFloat(existed_item.quantity.toFixed(2)),
                    };
                }
            } else {
                if (action.payload.quantity > action.payload.stock) {
                    setTimeout(function() {
                        alert('สินค้าที่มีอยู่ไม่พอ');
                    }, 1000);
                    return state;
                } else {
                    item.sum_price = parseFloat(item.price.toFixed(2)) * parseFloat(item.quantity.toFixed(2));
                    return {
                        ...state,
                        item: [...state.item, item],
                        totalPrice: parseFloat(state.totalPrice.toFixed(2)) + parseFloat(item.sum_price.toFixed(2)),
                    };
                }
            }

        // ---------- remove item ---------- //
        case REMOVE_ITEM_FROM_CART:
            let itemToRemove = state.item.find(item => item.uid === action.payload);
            let newItem = state.item.filter(item => item.uid !== action.payload);
            return {
                ...state,
                item: newItem,
                totalPrice: parseFloat(state.totalPrice.toFixed(2)) - parseFloat(itemToRemove.sum_price.toFixed(2)),
            };

        case UPDATE_STOCK:
            new_stock = state.item.map(item => (item.uid === action.payload.uid ? { ...item, stock: item.stock + action.payload.stock } : item));
            return {
                ...state,
                item: new_stock,
            };

        case CLEAR_CART:
            return {
                ...state,
                item: [],
                totalPrice: 0,
            };
        default:
            return state;
    }
};

export default OrderReducer;
