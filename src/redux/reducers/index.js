import { combineReducers } from 'redux';

import OrderReducer from './OrderReducer';
import StockReducer from './StockReducer';

export default combineReducers({
    order: OrderReducer,
    stock: StockReducer,
});
