import { TOGGLE_EDIT } from '../actions/ActionType.js';

const initState = {
    item: [],
    toggle_edit: false,
};

const OrderReducer = (state = initState, action) => {
    switch (action.type) {
        case TOGGLE_EDIT:
            return {
                ...state,
                toggle_edit: !action.payload,
            };
        default:
            return state;
    }
};

export default OrderReducer;
