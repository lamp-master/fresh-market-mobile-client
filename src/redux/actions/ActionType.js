export const ADD_ITEM_TO_CART = 'add_item_to_cart';
export const REMOVE_ITEM_FROM_CART = 'remove_item_from_cart';
export const ADD_QUANTITY = 'add_quantity';
export const SUB_QUANTITY = 'sub_quantity';
export const CLEAR_CART = 'clear_cart';
export const UPDATE_STOCK = 'update_stock';

export const TOGGLE_EDIT = 'toggle_edit';
