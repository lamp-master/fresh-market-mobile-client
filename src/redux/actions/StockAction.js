import { TOGGLE_EDIT } from './ActionType';

export const toggleEdit = toggle => {
    return dispatch => {
        dispatch({ type: TOGGLE_EDIT, payload: toggle });
    };
};
