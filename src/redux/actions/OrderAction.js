import { ADD_ITEM_TO_CART, SUB_QUANTITY, ADD_QUANTITY, REMOVE_ITEM_FROM_CART, CLEAR_CART, UPDATE_STOCK } from './ActionType';

export const addItemToCart = obj => {
    return dispatch => {
        let order = {
            uid: obj.uid,
            name: obj.name,
            price: obj.price,
            type: obj.type,
            quantity: obj.quantity,
            stock: obj.stock,
        };
        dispatch({ type: ADD_ITEM_TO_CART, payload: order });
    };
};
export const removeItemFromCart = uid => {
    return dispatch => {
        dispatch({ type: REMOVE_ITEM_FROM_CART, payload: uid });
    };
};

export const addQuantity = uid => {
    return dispatch => {
        dispatch({ type: ADD_QUANTITY, payload: uid });
    };
};

export const subQuantity = uid => {
    return dispatch => {
        dispatch({ type: SUB_QUANTITY, payload: uid });
    };
};

export const clearCart = () => {
    return dispatch => {
        dispatch({ type: CLEAR_CART, payload: null });
    };
};

export const updateStock = (id, stock) => {
    return dispatch => {
        let obj = {
            uid: id,
            stock: stock,
        };
        dispatch({ type: UPDATE_STOCK, payload: obj });
    };
};
