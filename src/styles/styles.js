import { StyleSheet } from 'react-native';

// DEFAULT COLOR
export const primaryColor = {
    green: '#425911',
    brown: '#5E1214',
    blue: '#2E0F40',
};
export const secondaryColor = {
    green: '#93AC5F',
    brown: '#AF6163',
    blue: '#865C9F',
};

// DEFAULT STYLE
export const styles = StyleSheet.create({
    // CONTANER
    container: {
        flex: 1,
        alignItems: 'stretch',
        justifyContent: 'center',
    },
    rowContainer: {
        flex: 1,
        flexDirection: 'row',
    },

    row: {
        flexDirection: 'row',
        alignSelf: 'stretch',
        alignItems: 'center',
        justifyContent: 'center',
    },

    // FORM
    formContainer: {
        margin: 10,
        padding: 20,
        justifyContent: 'center',

        borderRadius: 20,
        borderWidth: 2,
        borderColor: 'lightgrey',

        backgroundColor: 'rgba(211, 211, 211, 0.70)',
    },

    formElement: {
        margin: 5,
        alignItems: 'center',
    },
    formRowElement: {
        margin: 5,
        flexDirection: 'row',
        justifyContent: 'center',
    },

    // CARD
    cardContainer: {
        margin: 10,
        padding: 20,
        justifyContent: 'center',

        borderRadius: 20,
        borderWidth: 2,
        borderColor: 'lightgrey',

        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },

    // TEXT INPUT
    inputContainer: {
        flex: 1,
    },
    inputPrimary: {
        height: 45,
        width: '100%',
        padding: 5,
        alignSelf: 'stretch',
        paddingLeft: 15,
        paddingRight: 15,
        borderRadius: 20,
        borderColor: '#404040',
        borderWidth: 2,
    },

    // TEXT
    textTitle: {
        fontSize: 29,
        fontWeight: 'bold',
    },
    textSubtitle: {
        fontSize: 18,
        color: 'darkgrey',
    },
    textH1: {
        fontSize: 23,
    },
    textH2: {
        fontSize: 19,
    },
    textH3: {
        fontSize: 17,
    },
    textH4: {
        fontSize: 15,
    },
    textNormal: {
        fontSize: 14,
    },

    // SHADOW
    shadow: {
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },
});
